<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
            $this->load->model('users_model');
			$this->load->model('serviceslist_model');
			$this->load->model('spprofileimage_model');
			$this->load->model('spregistration_model');
			$this->load->model('spregistrationsteps_model');
			$this->load->model('spservicelocation_model');
			$this->load->model('spqualification_model');
			$this->load->model('spidentityverification_model');
			$this->load->model('role_model');
			$this->load->model('otp_model');
			$this->load->model('chemistshopinformation_model');
			$this->load->model('chemistidentityverification_model');
			$this->load->model('ambulancedriverprofessional_model');
			$this->load->model('labinformation_model');
			$this->load->model('ambvehicledetails_model');
			$this->load->model('ambdrivervehiclemaping_model');
			
			$this->load->model('spbankdetails_model');
			$this->load->model('sppancard_model');
			
			$this->load->library('msg91');
            $this->load->helper("url_helper");
			$this->load->library('session');
			
			if(!$this->session->has_userdata("userdata")){
				redirect(base_url()."login");		
			}

    }

	public function index()
	{
		$this->load->view('backend/index');
	}

	public function logout(){
		$this->session->unset_userdata('userdata');
		redirect(base_url()."login");		
	}
	
	public function services(){
		$data=array();
		$msg = intval($this->input->get("msg"));
		switch($msg){
			case 1:
				$msg = "Services has been saved successfully";
				break;
			case 2:
				$msg = "Services has been updated successfully";
				break;
			case 3:
				$msg = "Services has been deleted successfully";
				break;
			case 4:
				$msg="There is some error...";
				break;
			default:
				$msg=null;
				break;
		}
		$data["msg"]=$msg;
		$allservices = $this->db->order_by("serviceslist.id desc")->get_where('serviceslist',array('isdelete'=>1))->result();
		$data['services'] = $allservices;
		$this->load->view("backend/services",$data);
	}
	
	public function add_services(){
		$data=array();
		$this->load->view("backend/services_add",$data);
	}
	
	public function create_services(){
		extract($this->input->post());
		$data = array();
		$insert_services = array(
			"name" => $services_name
		);	
		$insert = $this->db->insert('serviceslist',$insert_services);
		if($insert){
			redirect(base_url()."dashboard/services?msg=3");
		}else{
			redirect(base_url()."services/services?msg=4");
		}
	}
	
	public function editServices($id){
		$data = array();
		$services_details = $this->db->get_where('serviceslist',array('id'=>$id))->row();
		$data['service'] = $services_details;
		$this->load->view("backend/services_edit",$data);
	}
	
	public function update_services(){
		extract($this->input->post());
		
		$data = array();
		$update_services = array(
			"name" => $services_name
		);
		$this->db->where('id',$services_id);
		$update = $this->db->update('serviceslist',$update_services);
		if($update){
			redirect(base_url()."dashboard/services?msg=2");
		}else{
			redirect(base_url()."services/services?msg=4");
		}
		
	}
	
	
	public function deleteServices($id){
		extract($this->input->post());
		$data = array();
		$delete_services = array(
			"isdelete" => 0,
		);
		$this->db->where('id',$id);
		$delete_address = $this->db->update('serviceslist',$delete_services);
		if($delete_address){
			redirect(base_url()."dashboard/services?msg=3");
		}else{
			redirect(base_url()."services/services?msg=4");
		}
	}
	
	public function allservice_providers(){
		$data = array();
		$allservices = $this->db->get_where("serviceprovider_profile_registration",array('status'=>1))->result();
		$data['sp_list'] = $allservices;
		$this->load->view("backend/service_provider_lists",$data);
	}

	public function serviceproviders_details($profile_id){
		
		$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
		$profile_data = array();
		if($sp_registration_data){
			$profile_data['profile_reg']['profile_id'] = $sp_registration_data->id;
			$profile_data['profile_reg']['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
			$profile_data['profile_reg']['servicetype_id'] = $sp_registration_data->servicetype_id;
			$profile_data['profile_reg']['mobile_no'] = $sp_registration_data->mobile_no;
			$profile_data['profile_reg']['first_name'] = $sp_registration_data->first_name;
			$profile_data['profile_reg']['middle_name'] = $sp_registration_data->middle_name;
			$profile_data['profile_reg']['last_name'] = $sp_registration_data->last_name;
			$profile_data['profile_reg']['date_of_birth'] = date("d-m-y",strtotime($sp_registration_data->date_of_birth));
			$profile_data['profile_reg']['address'] = $sp_registration_data->address;
			$profile_data['profile_reg']['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
			$profile_data['profile_reg']['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
			$profile_data['profile_reg']['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
			if($sp_registration_data->is_declaration == 1){
				$profile_data['profile_reg']['declaration_status'] = "Yes";
			}else{
				$profile_data['profile_reg']['declaration_status'] = "No";
			}
			if($sp_registration_data->gender == 1){
				$profile_data['profile_reg']['gender'] = "Male";
			}else{
				$profile_data['profile_reg']['gender'] = "Female";
			}
		}else{
			$profile_data['profile_reg'] = [];
		}
		$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
		if($sp_location){
			$profile_data['sldata']['service_location'] = $sp_location->service_location;
			$profile_data['sldata']['service_distance'] = $sp_location->service_distance." km";
			$profile_data['sldata']['service_location'] = $sp_location->service_location;
		}else{
			$profile_data['sldata'] = [];
		}
		$qualification = $this->db->get_where('spqualification',array('service_provider_id'=>$profile_id))->row();
		
		$sp_driver_pro_qua = $this->db->get_where("ambulancedriverprofessional",array('service_provider_id'=>$profile_id))->row();
		if($sp_driver_pro_qua){
			$profile_data['driver_pro_qua']['name'] = $sp_driver_pro_qua->name;
			$profile_data['driver_pro_qua']['driving_license_number'] = $sp_driver_pro_qua->driving_license_number;
			$profile_data['driver_pro_qua']['dl_photo'] = $this->spprofileimage_model->showprofileimage($sp_driver_pro_qua->dl_photo);
		}else{
			$profile_data['driver_pro_qua'] = [];
		}
		
		if($qualification){
			$profile_data['sp_qualification']['qualification_name'] = $qualification->qualification_name;
			$profile_data['sp_qualification']['additional_qualification_details'] = $qualification->additional_qualification;
			$profile_data['sp_qualification']['registration_number'] = $qualification->registration_number;
			$profile_data['sp_qualification']['certification_attachment'] = $this->spprofileimage_model->showprofileimage($qualification->certification_attachment);
			$profile_data['sp_qualification']['year_of_experience'] = $qualification->year_of_experience;
		}else{
			$profile_data['sp_qualification'] = [];
		}
		$identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
		if($identityverification){
			$profile_data['identityverification']['identification_name'] = $identityverification->identification_name;
			$profile_data['identityverification']['identification_number'] = $identityverification->identification_number;
			$profile_data['identityverification']['identification_front_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_front_photo);
			$profile_data['identityverification']['identification_back_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_back_photo);
		}else{
			$profile_data['identityverification'] = [];
		}
		
		$banking_details = $this->db->get_where("spbankdetails",array('service_provider_id'=>$profile_id))->row();
		if($banking_details){
			$profile_data['banking_data']['bank_name'] = $banking_details->sp_name;
			$profile_data['banking_data']['account_number'] = $banking_details->account_number;
			$profile_data['banking_data']['ifsc_code'] = $banking_details->ifsc_code;
			$profile_data['banking_data']['cancel_cheque_photo'] = $this->spprofileimage_model->showprofileimage($banking_details->cancel_cheque_photo);
		}else{
			$profile_data['banking_data'] = [];
		}
		$gst_details = $this->db->get_where("sppancard",array('service_provider_id'=>$profile_id))->row();
		if($gst_details){
			$profile_data['gst_data']['pancard_number'] = $gst_details->pancard_number;
			$profile_data['gst_data']['name'] = $gst_details->sp_name;
			$profile_data['gst_data']['gst_number'] = $gst_details->gst_number;
			$profile_data['gst_data']['gst_document_photo'] = $this->spprofileimage_model->showprofileimage($gst_details->gst_document_photo);
		}else{
			$profile_data['gst_data'] = [];
		}
		$vehicle_details = $this->db->get_where("ambvehicledetails",array('service_provider_id'=>$profile_id))->result();
		if($vehicle_details){
			$veh_details = array();
			foreach($vehicle_details as $key1=>$value1){
				$veh_details[$key1]['vehicle_name'] = $value1->vehicle_name;
				$veh_details[$key1]['vehicle_type'] = $value1->vehicle_type;
				$veh_details[$key1]['registration_date'] = $value1->registration_date;
				$veh_details[$key1]['vehicle_reg_no'] = $value1->vehicle_reg_no;
				$veh_details[$key1]['reg_certificate_photo'] = $this->spprofileimage_model->showprofileimage($value1->reg_certificate_photo);
			}
			$profile_data['vehicle_details']['all_vehicles'] = $veh_details;
		}else{
			$profile_data['vehicle_details'] = [];
		}
		$vehicle_deriver = $this->db->get_where("ambdrivervehiclemaping",array('service_provider_id'=>$profile_id))->result();
		if($vehicle_deriver){
			$vehicle_driver = array();
			foreach($vehicle_deriver as $key => $value){
				$veh_det = $this->db->get_where("ambvehicledetails",array('id'=>$value->ambvehicledetails_id))->row();
				$dri_det = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$value->ambulance_driver_id))->row();
				$vehicle_driver[$key]['driver_name'] = $dri_det->first_name." ".$dri_det->middle_name." ".$dri_det->last_name;
				$vehicle_driver[$key]['vehicle_name'] = $veh_det->vehicle_name;
			}
			$profile_data['vehicle_driver_details'] = $vehicle_driver;
		}else{
			$profile_data['vehicle_driver_details'] = [];
		}
		$lab_info = $this->db->get_where("labinformation",array('service_provider_id'=>$profile_id))->row();
		if($lab_info){
			$profile_data['lab_info']['lab_name'] = $lab_info->lab_name;
			$profile_data['lab_info']['lab_profile_picture'] = $this->spprofileimage_model->showprofileimage($lab_info->lab_profile_picture);
			$profile_data['lab_info']['lab_mobile_no'] = $lab_info->lab_mobile_no;
			$profile_data['lab_info']['lab_email_id'] = $lab_info->lab_email_id;
			$profile_data['lab_info']['lab_address_proof_type'] = $lab_info->lab_address_proof_type;
			$profile_data['lab_info']['lab_upload_document'] = $this->spprofileimage_model->showprofileimage($lab_info->lab_upload_document);
		}else{
			$profile_data['lab_info'] = [];
		}
		$chemist = $this->db->get_where("chemistshopinformation",array('service_provider_id'=>$profile_id))->row();
		if($chemist){
			$profile_data['chemist']['drug_licence_no'] = $chemist->drug_licence_no;
			$profile_data['chemist']['drug_licence_photo'] = $this->spprofileimage_model->showprofileimage($chemist->drug_licence_photo);
			$profile_data['chemist']['pan_no'] = $chemist->pan_no;
			$profile_data['chemist']['pan_photo'] = $this->spprofileimage_model->showprofileimage($chemist->pan_photo);
			$profile_data['chemist']['gst_no'] = $chemist->gst_no;
			$profile_data['chemist']['gst_document'] = $this->spprofileimage_model->showprofileimage($chemist->gst_document);
		}else{
			$profile_data['chemist'] = [];
		}
		$sp_bankdetails = $this->db->get_where("spbankdetails",array('service_provider_id'=>$profile_id))->row();
		if($sp_bankdetails){
			$profile_data['bank_details']['sp_name'] = $sp_bankdetails->sp_name;
			$profile_data['bank_details']['account_number'] = $sp_bankdetails->account_number;
			$profile_data['bank_details']['ifsc_code'] = $sp_bankdetails->ifsc_code;
			$profile_data['bank_details']['cancel_cheque_photo'] = $this->spprofileimage_model->showprofileimage($sp_bankdetails->cancel_cheque_photo);
		}else{
			$profile_data['bank_details'] = [];
		}
		
		$sp_gstdetails = $this->db->get_where("sppancard",array('service_provider_id'=>$profile_id))->row();
		if($sp_gstdetails){
			$profile_data['gst_details']['pancard_number'] = $sp_gstdetails->pancard_number;
			$profile_data['gst_details']['sp_name'] = $sp_gstdetails->sp_name;
			$profile_data['gst_details']['gst_number'] = $sp_gstdetails->gst_number;
			$profile_data['gst_details']['gst_document_photo'] = $this->spprofileimage_model->showprofileimage($sp_gstdetails->gst_document_photo);
		}else{
			$profile_data['gst_details'] = [];
		}
		
		$data = array();
		$data['result'] = $profile_data;
		$this->load->view("backend/serviceprovider_details",$data);
	}
	
	public function profile_approve(){
		$post = $this->input->post();
		$service_provider_id = $post['profile_id'];
		$check_profile_complete = $this->spprofileimage_model->getcompletepercentage($service_provider_id);
		$details = $this->db->query("select * from spregistration_steps where service_provider_id='$service_provider_id'")->row();
		$send_request = $details->isverified;
		if($check_profile_complete == 100){
			if($send_request == 1){
				$this->db->where('service_provider_id',$service_provider_id);
				$update_status = $this->db->update('spregistration_steps',array('isverified'=>2));
				if($update_status){
					$status = 1;
					$msg = "Approve for face to face verification";
				}else{
					$status = 0;
					$msg = "There is some error while update the record";
				}
			}else{
				$status = 0;
				$msg = "Request not send.Please send the request for approval..";
			}
		}else{
			$status = 0;
			$msg = "Profile not completed...";
		}
		
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function setfacetofaceverification(){
		$post = $this->input->post();
		$service_provider_id = $post['profile_id'];
		$choose_datetime = $post['choosen_datetime'];
		$address = $post['address'];
		$check_profile_complete = $this->spprofileimage_model->getcompletepercentage($service_provider_id);
		$details = $this->db->query("select * from spregistration_steps where service_provider_id='$service_provider_id'")->row();
		$send_request = $details->isverified;
		if($check_profile_complete == 100){
			if($send_request == 1){
				$php_format = date("Y-m-d H:i:s",strtotime($choose_datetime));
				$this->db->where('service_provider_id',$service_provider_id);
				$update_status = $this->db->update('spregistration_steps',array('facetofaceverification'=>$php_format,'address_facetoface'=>$address,'isverified'=>2));
				if($update_status){
					$details  = $this->db->get_where('serviceprovider_profile_registration',array('id'=>$service_provider_id))->row();
					$set_datetime = date("d-M-y H:i:s",strtotime($choose_datetime));
					$data['msg'] = "Congratulation!!!Your profile has been accepted and requested to come on ".$set_datetime." for physicalverification. For more details go to  app dashboard.";
					$data['mobile'] = $details->mobile_no;
					$smssend=$this->msg91->sendSms($data);
					
					if($smssend == 1){
						$status =1;
						$msg = "Face to face varification datetime schedule successfully..";
					}else{
						$status = 0;
						$msg = "There is some error while sending the sms...";
					}
				}else{
					$status = 0;
					$msg = "There is some error while setting the date time..";
				}
			}else if($send_request == 0){
				$status = 0;
				$msg = "Please send the verification requestion for verification...";
			}
			else{
				$status = 0;
				$msg = "There is some error while processing the request..";
			}
		}else{
			$status = 0;
			$msg = "Profile registration not completed.";
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
		
	}
	
	public function rejectVerification(){
		$post = $this->input->post();
		$service_provider_id = $post['profile_id'];
		$check_profile_complete = $this->spprofileimage_model->getcompletepercentage($service_provider_id);
		$details = $this->db->query("select * from spregistration_steps where service_provider_id='$service_provider_id'")->row();
		$send_request = $details->isverified;
		if($check_profile_complete == 100){
			$rejection_datetime = date("Y-m-d H:i:s");
			$this->db->where('service_provider_id',$service_provider_id);
			$update_status = $this->db->update('spregistration_steps',array('isverified'=>-1,'datetime_rejection'=>$rejection_datetime));
			if($update_status){
				$details  = $this->db->get_where('serviceprovider_profile_registration',array('id'=>$service_provider_id))->row();
				$data['msg'] = "Your Profile verification request has been rejected. Please contact to support for more details..";
				$data['mobile'] = $details->mobile_no;
				$smssend=$this->msg91->sendSms($data);
				
				if($smssend == 1){
					$status =1;
					$msg = "Reject successfully";
				}else{
					$status = 0;
					$msg = "There is some error while sending the sms...";
				}
			}else{
				$status = 0;
				$msg = "There is some error while reject the notification...";
			}
			
		}else{
			$status = 0;
			$msg = "Profile registration not completed.";
		}
		echo json_encode(array('status'=>$status,'msg'=>$msg));
	}
	
	public function completeVerification(){
		$post = $this->input->post();
		$service_provider_id = $post['profile_id'];
		$check_profile_complete = $this->spprofileimage_model->getcompletepercentage($service_provider_id);
		$details = $this->db->query("select * from spregistration_steps where service_provider_id='$service_provider_id'")->row();
		$send_request = $details->isverified;
		if($check_profile_complete == 100){
			$agency_status = $this->spprofileimage_model->agencyStatus($service_provider_id);
			if($agency_status == 2){
				$this->db->where('service_provider_id',$service_provider_id);
				$profile_complete = $this->db->update('spregistration_steps',array('isverified'=>3));
				if($profile_complete){
					$details  = $this->db->get_where('serviceprovider_profile_registration',array('id'=>$service_provider_id))->row();
					$data['msg'] = "Congratulation!!!Your profile has been activated successfully....";
					$data['mobile'] = $details->mobile_no;
					$smssend=$this->msg91->sendSms($data);
					
					if($smssend == 1){
						$status =1;
						$msg = "Profile verification completed successfully..";
					}else{
						$status = 0;
						$msg = "There is some error while sending the sms...";
					}
					
				}else{
					$status = 0 ;
					$msg = "There is some error while complete the process. Please try again...";
				}
				
			}else{
				$status = 0 ;
				$msg = "There is some error while complete the process. Either request not send or profile not complete..";
			}
		}else{
			$status = 0 ;
			$msg = "Profile not completed yet. Please complete the Profile..";
		}
		
		echo json_encode(array("status" => $status, "msg" => $msg));
	}
	
	public function resetDb(){
		$this->db->truncate('ambdrivervehiclemaping');
		$this->db->truncate('ambulancedriverprofessional');
		$this->db->truncate('ambvehicledetails');
		$this->db->truncate('chemistidentityverification');
		$this->db->truncate('labinformation');
		$this->db->truncate('serviceprovider_profile_registration');
		$this->db->truncate('spbankdetails');
		$this->db->truncate('spidentity_verification');
		$this->db->truncate('sppancard');
		$this->db->truncate('spqualification');
		$this->db->truncate('spregistration_steps');
		$this->db->truncate('spservice_location');
		$this->db->truncate('sp_profile_image');
		$this->load->view('backend/index');
	}
	
	
}
?>
