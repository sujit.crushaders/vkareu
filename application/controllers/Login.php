<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
            $this->load->model('users_model');
            
            $this->load->helper("url_helper");
			$this->load->library('session');
			

    }
    
	public function index()
	{
        if(isset($_POST['username']) && isset($_POST['password'])){
			
            $username = $this->input->post()['username'];
			$password = $this->input->post()['password'];

            $sha1_password = sha1($password);
			$login = $this->db->query("select * from users where username='$username' and password='$sha1_password' and isdelete='1'")->row();
			
			if($login){
				
				$this->session->set_userdata('userdata', $login);
				redirect(base_url()."dashboard");
			}else{
				
				$data['msg'] = "Invalid username or Password";
                $this->load->view('backend/login',$data);
			}
        }else{
			$this->load->view('backend/login');
        }
    } 
	
}
