<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Serviceproviderapi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('users_model');
		$this->load->model('serviceslist_model');
		$this->load->model('spprofileimage_model');
		$this->load->model('spregistration_model');
		$this->load->model('spregistrationsteps_model');
		$this->load->model('spservicelocation_model');
		$this->load->model('spqualification_model');
		$this->load->model('spidentityverification_model');
		$this->load->model('role_model');
		$this->load->model('otp_model');
		$this->load->model('chemistshopinformation_model');
		$this->load->model('chemistidentityverification_model');
		$this->load->model('ambulancedriverprofessional_model');
		$this->load->model('labinformation_model');
		$this->load->model('ambvehicledetails_model');
		$this->load->model('ambulancedriverprofessional_model');
		$this->load->model('ambdrivervehiclemaping_model');
		
		$this->load->model('spbankdetails_model');
		$this->load->model('sppancard_model');
		
		$this->load->library('msg91');
		$this->load->helper("url_helper");
			
    }

	public function serviceLists(){
		$allservices = $this->db->order_by("id","asc")->get_where('serviceslist',array('isdelete'=>1))->result();
		if($allservices){
			$status = 1;
			$msg = "Services list  available";
			$data = $allservices;
		}else{
			$status = 0;
			$msg = "Services list not available";
			$data = [];
		}
		$json = array();
		$json['status'] = $status;
		$json['msg'] = $msg;
		$json['data'] = $data;
		echo json_encode($json);
	}
	
	public function sendOtp(){
		
		$post = $this->input->post();
		extract($post);
		if($mobile_no!=''){
			$otp_code = mt_rand(1000,9999);
			$mobile_sms = array(
					"mobile" => $mobile_no,
					"otp" => $otp_code
				);
			$otp_status=$this->msg91->sendOtp($mobile_sms);
			$otp = $otp_code;
			$otp_insert = array(
				"mobile_no" => $mobile_no,
				"otp" => $otp,
			);
			$insert_otp = $this->db->insert("otp_verification",$otp_insert);
			if($insert_otp){
				$status = 1;
				$msg = "Otp Send to your Mobile No..";
				$data['mobile'] = $mobile_no;
				$data['otp_code'] = $otp;
			}else{
				$status = 0;
				$msg = "There is some error while insert the data in otp table.";
				$data = [];
			}
		}else{
			$status = 0;
			$otp = "";
			$msg = "Enter the Mobile No..";
		}
		$json = array();
		$json['status'] = $status;
		$json['msg'] = $msg;
		echo json_encode($json);
	}
	
	public function resendOtp(){
		$post = $this->input->post();
		$mobile = $post['mobile_no'];
		$last_otp = $this->db->get_where("otp_verification",array('mobile_no'=>$mobile,'isverified'=>0))->row();
		if($last_otp){
			$otp_code = $last_otp->otp;
			$mobile_sms = array(
					"mobile" => $mobile,
					"otp" => $otp_code
				);
			$otp_status=$this->msg91->sendOtp($mobile_sms);
			$status = 1;
			$msg = "Otp Send to your Mobile No..";
			$data['mobile'] = $mobile;
			$data['otp_code'] = $otp_code;
		}else{
			$otp_code_gen = mt_rand(1000,9999);
			$mobile_sms = array(
					"mobile" => $mobile,
					"otp" => $otp_code_gen
				);
			$otp_status=$this->msg91->sendOtp($mobile_sms);
			$otp_insert = array(
				"mobile_no" => $mobile,
				"otp" => $otp_code_gen,
			);
			$insert_otp = $this->db->insert("otp_verification",$otp_insert);
			if($insert_otp){
				$status = 1;
				$msg = "Otp Send to your Mobile No..";
				$data['mobile'] = $mobile;
				$data['otp_code'] = $otp_code_gen;
			}else{
				$status = 0;
				$msg = "There is some error while insert the data in otp table.";
				$data = [];
			}
		}
		
		$json = array();
		$json['status'] = $status;
		$json['msg'] = $msg;
		$json['data'] = $data;
		echo json_encode($json);
	}
	
	public function otpVerification(){
		$post = $this->input->post();
		$mobile_no = $post['mobile_no'];
		$otp_code = $post['otp_code'];
		$otp_match = $this->db->get_where("otp_verification",array("mobile_no"=>$mobile_no,"otp"=>$otp_code,"isverified"=>0))->row();
		if($otp_match){
			$id = $otp_match->id;
			$this->db->where('id',$id);
			$verified_update = $this->db->update("otp_verification",array("isverified"=>1));
			if($verified_update){
				$check_sp = $this->db->get_where('serviceprovider_profile_registration',array('mobile_no'=>$mobile_no))->row();
				if($check_sp){
					$profile_id = $check_sp->id;
					
					$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
					$profile_data = array();
					if($sp_registration_data){
						$profile_data['profile_reg']['profile_id'] = $sp_registration_data->id;
						$profile_data['profile_reg']['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
						$profile_data['profile_reg']['servicetype_id'] = $sp_registration_data->servicetype_id;
						$profile_data['profile_reg']['mobile_no'] = $sp_registration_data->mobile_no;
						$profile_data['profile_reg']['first_name'] = $sp_registration_data->first_name;
						$profile_data['profile_reg']['middle_name'] = $sp_registration_data->middle_name;
						$profile_data['profile_reg']['last_name'] = $sp_registration_data->last_name;
						$profile_data['profile_reg']['date_of_birth'] = date("d-m-y",strtotime($sp_registration_data->date_of_birth));
						$profile_data['profile_reg']['address'] = $sp_registration_data->address;
						$profile_data['profile_reg']['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
						$profile_data['profile_reg']['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
						$profile_data['profile_reg']['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
						if($sp_registration_data->is_declaration == 1){
							$profile_data['profile_reg']['declaration_status'] = "Yes";
						}else{
							$profile_data['profile_reg']['declaration_status'] = "No";
						}
						if($sp_registration_data->gender == 1){
							$profile_data['profile_reg']['gender'] = "Male";
						}else{
							$profile_data['profile_reg']['gender'] = "Female";
						}
					}else{
						$profile_data['profile_reg'] = [];
					}
					$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
					if($sp_location){
						$profile_data['sldata']['service_location'] = $sp_location->service_location;
						$profile_data['sldata']['service_distance'] = $sp_location->service_distance." km";
						$profile_data['sldata']['service_location'] = $sp_location->service_location;
					}else{
						$profile_data['sldata'] = [];
					}
					$qualification = $this->db->get_where('spqualification',array('service_provider_id'=>$profile_id))->row();
					
					$sp_driver_pro_qua = $this->db->get_where("ambulancedriverprofessional",array('service_provider_id'=>$profile_id))->row();
					if($sp_driver_pro_qua){
						$profile_data['driver_pro_qua']['name'] = $sp_driver_pro_qua->name;
						$profile_data['driver_pro_qua']['driving_license_number'] = $sp_driver_pro_qua->driving_license_number;
						$profile_data['driver_pro_qua']['dl_photo'] = $this->spprofileimage_model->showprofileimage($sp_driver_pro_qua->dl_photo);
					}else{
						$profile_data['driver_pro_qua'] = [];
					}
					
					if($qualification){
						$profile_data['sp_qualification']['qualification_name'] = $qualification->qualification_name;
						$profile_data['sp_qualification']['additional_qualification_details'] = $qualification->additional_qualification;
						$profile_data['sp_qualification']['registration_number'] = $qualification->registration_number;
						$profile_data['sp_qualification']['certification_attachment'] = $this->spprofileimage_model->showprofileimage($qualification->certification_attachment);
						$profile_data['sp_qualification']['year_of_experience'] = $qualification->year_of_experience;
					}else{
						$profile_data['sp_qualification'] = [];
					}
					$identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
					if($identityverification){
						$profile_data['identityverification']['identification_name'] = $identityverification->identification_name;
						$profile_data['identityverification']['identification_number'] = $identityverification->identification_number;
						$profile_data['identityverification']['identification_front_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_front_photo);
						$profile_data['identityverification']['identification_back_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_back_photo);
					}else{
						$profile_data['identityverification'] = [];
					}
					
					$banking_details = $this->db->get_where("spbankdetails",array('service_provider_id'=>$profile_id))->row();
					if($banking_details){
						$profile_data['banking_data']['bank_name'] = $banking_details->sp_name;
						$profile_data['banking_data']['account_number'] = $banking_details->account_number;
						$profile_data['banking_data']['ifsc_code'] = $banking_details->ifsc_code;
						$profile_data['banking_data']['cancel_cheque_photo'] = $this->spprofileimage_model->showprofileimage($banking_details->cancel_cheque_photo);
					}else{
						$profile_data['banking_data'] = [];
					}
					$gst_details = $this->db->get_where("sppancard",array('service_provider_id'=>$profile_id))->row();
					if($gst_details){
						$profile_data['gst_data']['pancard_number'] = $gst_details->pancard_number;
						$profile_data['gst_data']['name'] = $gst_details->sp_name;
						$profile_data['gst_data']['gst_number'] = $gst_details->gst_number;
						$profile_data['gst_data']['gst_document_photo'] = $this->spprofileimage_model->showprofileimage($gst_details->gst_document_photo);
					}else{
						$profile_data['gst_data'] = [];
					}
					$vehicle_details = $this->db->get_where("ambvehicledetails",array('service_provider_id'=>$profile_id))->result();
					if($vehicle_details){
						$veh_details = array();
						foreach($vehicle_details as $key1=>$value1){
							$veh_details[$key1]['vehicle_name'] = $value1->vehicle_name;
							$veh_details[$key1]['vehicle_type'] = $value1->vehicle_type;
							$veh_details[$key1]['registration_date'] = $value1->registration_date;
							$veh_details[$key1]['vehicle_reg_no'] = $value1->vehicle_reg_no;
							$veh_details[$key1]['reg_certificate_photo'] = $this->spprofileimage_model->showprofileimage($value1->reg_certificate_photo);
						}
						$profile_data['vehicle_details']['all_vehicles'] = $veh_details;
					}else{
						$profile_data['vehicle_details'] = [];
					}
					$vehicle_deriver = $this->db->get_where("ambdrivervehiclemaping",array('service_provider_id'=>$profile_id))->result();
					if($vehicle_deriver){
						$vehicle_driver = array();
						foreach($vehicle_deriver as $key => $value){
							$veh_det = $this->db->get_where("ambvehicledetails",array('id'=>$value->ambvehicledetails_id))->row();
							$dri_det = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$value->ambulance_driver_id))->row();
							$vehicle_driver[$key]['driver_name'] = $dri_det->first_name." ".$dri_det->middle_name." ".$dri_det->last_name;
							$vehicle_driver[$key]['vehicle_name'] = $veh_det->vehicle_name;
						}
						$profile_data['vehicle_driver_details'] = $vehicle_driver;
					}else{
						$profile_data['vehicle_driver_details'] = [];
					}
					$lab_info = $this->db->get_where("labinformation",array('service_provider_id'=>$profile_id))->row();
					if($lab_info){
						$profile_data['lab_info']['lab_name'] = $lab_info->lab_name;
						$profile_data['lab_info']['lab_profile_picture'] = $this->spprofileimage_model->showprofileimage($lab_info->lab_profile_picture);
						$profile_data['lab_info']['lab_mobile_no'] = $lab_info->lab_mobile_no;
						$profile_data['lab_info']['lab_email_id'] = $lab_info->lab_email_id;
						$profile_data['lab_info']['lab_address_proof_type'] = $lab_info->lab_address_proof_type;
						$profile_data['lab_info']['lab_upload_document'] = $this->spprofileimage_model->showprofileimage($lab_info->lab_upload_document);
					}else{
						$profile_data['lab_info'] = [];
					}
					$chemist = $this->db->get_where("chemistshopinformation",array('service_provider_id'=>$profile_id))->row();
					if($chemist){
						$profile_data['chemist']['drug_licence_no'] = $chemist->drug_licence_no;
						$profile_data['chemist']['drug_licence_photo'] = $this->spprofileimage_model->showprofileimage($chemist->drug_licence_photo);
						$profile_data['chemist']['pan_no'] = $chemist->pan_no;
						$profile_data['chemist']['pan_photo'] = $this->spprofileimage_model->showprofileimage($chemist->pan_photo);
						$profile_data['chemist']['gst_no'] = $chemist->gst_no;
						$profile_data['chemist']['gst_document'] = $this->spprofileimage_model->showprofileimage($chemist->gst_document);
					}else{
						$profile_data['chemist'] = [];
					}

					$status = 1;
					$msg = "Record found";
					$data = $profile_data;
				}else{
					$status = 1;
					$msg = "New User";
					$data = [];
				}
				
			}else{
				$status = 0;
				$msg = "There is some error while update the verification data";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "Invalid OtpCode";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" => $msg,
			"data" => $data,
		));
	}
	
	public function decodebase64image($image){
		if($image!=""){
			$output_file = 'upload/';
			$output_file_with_extension = time().".png";
			file_put_contents( $output_file . $output_file_with_extension, base64_decode($image) );
			$file_path = base_url();
			$file_save_path = $file_path.$output_file.$output_file_with_extension;
			$profile_image_insert = array(
				"profile_image" => $file_save_path,
			);
			$image_insert = $this->db->insert("sp_profile_image",$profile_image_insert);
			if($image_insert){
				$image_id = $this->db->insert_id();
			}else{
				$image_id = "";
			}
		}else{
			$image_id ="";
		}
		return $image_id;
	}

	/* Service Provider Registration(Doctors,Nurse,Physiotherapists,Pharmacists,LabTechnicians */
	
	public function registration_step_one(){
		$post = $this->input->post();
		$mobile_no = 		$post['mobile_no'];
		$servicetype_id = 	$post['servicetype_id'];
		$first_name = 		$post['first_name'];
		if(isset($post['middle_name'])){
			$middle_name = 		$post['middle_name'];
		}else{
			$middle_name = "";
		}
		$last_name = 		$post['last_name'];
		$gender = 			$post['gender'];
		$date_of_birth = 	$post['date_of_birth'];
		$address = 			$post['address'];
		$latitude = 		$post['latitude'];
		$longitude = 		$post['longitude'];
		if(isset($post['email'])){
			$email = $post['email'];
		}else{
			$email = "";
		}
		if(isset($post['base64_profile_image'])){
			$profile_image = 	$post['base64_profile_image'];
			$image_id = $this->decodebase64image($profile_image);
		}else{
			$image_id =  "";
		}
		$sp_mobile_chk = $this->db->get_where('serviceprovider_profile_registration',array('mobile_no'=>$post['mobile_no'],'status'=>1))->row();
		if(!$sp_mobile_chk){
			$profile_insert = array(
				"mobile_no" => 			$post['mobile_no'],
				"servicetype_id" => 	$post['servicetype_id'],
				"first_name" => 		$post['first_name'],
				"middle_name" => 		$middle_name,
				"last_name" => 			$post['last_name'],
				"gender" => 			$post['gender'],
				"date_of_birth" => 		date("Y-m-d",strtotime($post['date_of_birth'])),
				"address" => 			$post['address'],
				"latitude" => 			$post['latitude'],
				"longitude" => 			$post['longitude'],
				"profileimage_id" => 	$image_id,
				"email" => 	$email,
			);
			$profile_create = $this->db->insert("serviceprovider_profile_registration",$profile_insert);
			if($profile_create){
				$service_provider_id = $this->db->insert_id();
				$steps_complete = array(
					"steps_complete" => 1,
					"service_provider_id" => $service_provider_id,
					"complete_percentage" => 20,
				);
				$steps_comp = $this->db->insert("spregistration_steps",$steps_complete);
				if($steps_comp){
					$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$service_provider_id))->row();
					$profile_data = array();
					$profile_data['profile_id'] = $sp_registration_data->id;
					$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
					$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
					$profile_data['first_name'] = $sp_registration_data->first_name;
					$profile_data['middle_name'] = $sp_registration_data->middle_name;
					$profile_data['last_name'] = $sp_registration_data->last_name;
					$profile_data['email'] = $sp_registration_data->email;
					$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
					$profile_data['address'] = $sp_registration_data->address;
					$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
					$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
					$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
					if($sp_registration_data->is_declaration == 1){
						$profile_data['declaration_status'] = "Yes";
					}else{
						$profile_data['declaration_status'] = "No";
					}
					if($sp_registration_data->gender == 1){
						$profile_data['gender'] = "Male";
					}else{
						$profile_data['gender'] = "Female";
					}
					$status = 1;
					$msg = "Success";
					$data = $profile_data;
				}else{
					$status = 0;
					$msg = "There is some error while insert the data in steps table";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Failure";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "Record exits.Please login using mobile no";
			$data = [];
		}
		
		
		echo json_encode(array(
			"status" 	=> $status,
			"msg" 		=> $msg,
			"data" 		=> $data
		));
	}

	public function servicelocationinsert(){
		$post = $this->input->post();
		$profile_id = $post['profile_id']; 
		$service_location = $post['service_location'];
		$service_distance = $post['service_distance'];
		$location_lat = $post['location_lat'];
		$location_lang = $post['location_lang'];
		$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
		if($check_sp_reg){
			$check_sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
			if(!$check_sp_location){
				$servicearea_insert = array(
					"service_location"		=> $service_location,
					"service_distance"		=> $service_distance,
					"location_lat"			=> $location_lat,
					"location_lang"			=> $location_lang,
					"service_provider_id" 	=> $profile_id,
				);
				$insert_servicelocation = $this->db->insert('spservice_location',$servicearea_insert);
				if($insert_servicelocation){
					$steps_complete_array = array(
						'steps_complete' => 2,
						'complete_percentage' => 40,
					);
					$this->db->where('service_provider_id',$profile_id);
					$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
					if($update_steps){
						$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
						$profile_data = array();
						$profile_data['profile_id'] = $sp_registration_data->id;
						$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
						$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
						$profile_data['first_name'] = $sp_registration_data->first_name;
						$profile_data['middle_name'] = $sp_registration_data->middle_name;
						$profile_data['last_name'] = $sp_registration_data->last_name;
						$profile_data['email'] = $sp_registration_data->email;
						$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
						$profile_data['address'] = $sp_registration_data->address;
						$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
						$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
						$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
						if($sp_registration_data->is_declaration == 1){
							$profile_data['declaration_status'] = "Yes";
						}else{
							$profile_data['declaration_status'] = "No";
						}
						if($sp_registration_data->gender == 1){
							$profile_data['gender'] = "Male";
						}else{
							$profile_data['gender'] = "Female";
						}
						$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
						$profile_data['service_location'] = $sp_location->service_location;
						$profile_data['service_distance'] = $sp_location->service_distance." km";
						$profile_data['service_location'] = $sp_location->service_location;
						$status  = 1;
						$msg  = "Service Location insert successfully";
						$data = $profile_data;
					}else{
						$status = 0;
						$msg = "There is some error while update the data in spregistration_steps table";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "There is some error while update the data in spservice_location table";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Record is exits in the spservice_location table..";
				$data = []; 
			}
		}else{
			$status = 0;
			$msg = "Record is not available in the serviceprovider_profile_registration..";
			$data = []; 
		}

		echo json_encode(array(
			"status" => $status,
			"msg"	 => $msg,
			"data"   => $data,
		));
	}

	public function spqualification_insert(){
		$post = $this->input->post();
		$profile_id = 					$post['profile_id']; 
		$qualification_name = 			$post['qualification_name']; 
		$registration_number = 			$post['registration_number']; 
		$certification_attachment = 	$post['certification_attachment']; 
		$year_of_experience = 			$post['year_of_experience'];
		if(isset($post['other_qualification'])){
			$other_qualification = $post['other_qualification'];
		}else{
			$other_qualification = "";
		}
		if(isset($post['additional_qualification_details'])){
			$additional = $post['additional_qualification_details'];
		}else{
			$additional = "";
		}
		
		$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
		if($check_sp_reg){
			$chk_qualification = $this->db->get_where('spqualification',array('service_provider_id'=>$profile_id))->row();
			if(!$chk_qualification){
				if(isset($post['certification_attachment'])){
					$certification_attachment = $post['certification_attachment'];
					$certificate_id = $this->decodebase64image($certification_attachment);
				}else{
					$certificate_id =  "";
				}
				
				if($certificate_id!=''){
					$qualification_insert = array(
						"qualification_name"  			=> $qualification_name,
						"additional_qualification"		=> $additional,
						"other_qualification_details"	=> $other_qualification,
						"registration_number"			=> $registration_number,
						"year_of_experience"			=> $year_of_experience,
						"service_provider_id"			=> $profile_id,
						"certification_attachment"		=> $certificate_id,
					);
					$qualification_save = $this->db->insert('spqualification',$qualification_insert);
					if($qualification_save){
						$steps_complete_array = array(
							'steps_complete' => 3,
							'complete_percentage' => 60,
						);
						$this->db->where('service_provider_id',$profile_id);
						$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
						if($update_steps){
							$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
							$profile_data = array();
							$profile_data['profile_id'] = $sp_registration_data->id;
							$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
							$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
							$profile_data['first_name'] = $sp_registration_data->first_name;
							$profile_data['middle_name'] = $sp_registration_data->middle_name;
							$profile_data['last_name'] = $sp_registration_data->last_name;
							$profile_data['email'] = $sp_registration_data->email;
							$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
							$profile_data['address'] = $sp_registration_data->address;
							$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
							$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
							$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
							if($sp_registration_data->is_declaration == 1){
								$profile_data['declaration_status'] = "Yes";
							}else{
								$profile_data['declaration_status'] = "No";
							}
							if($sp_registration_data->gender == 1){
								$profile_data['gender'] = "Male";
							}else{
								$profile_data['gender'] = "Female";
							}
							$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
							$profile_data['service_location'] = $sp_location->service_location;
							$profile_data['service_distance'] = $sp_location->service_distance." km";
							$profile_data['service_location'] = $sp_location->service_location;
							$qualification = $this->db->get_where('spqualification',array('service_provider_id'=>$profile_id))->row();
							$profile_data['qualification_name'] = $qualification->qualification_name;
							$profile_data['additional_qualification_details'] = $qualification->additional_qualification;
							$profile_data['other_qualification_details'] = $qualification->other_qualification_details;
							$profile_data['registration_number'] = $qualification->registration_number;
							$profile_data['certification_attachment'] = $this->spprofileimage_model->showprofileimage($qualification->certification_attachment);
							$profile_data['year_of_experience'] = $qualification->year_of_experience;
							$status  = 1;
							$msg  = "Qualification insert successfully";
							$data = $profile_data;
						}else{
							$status = 0;
							$msg = "There is some error while update the stepcomplete data..";
							$data = [];
						}
					}else{
						$status = 0;
						$msg = "There is some error while save the qualification data..";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "Please attach the qualification photo";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Service provider qualification data exits...";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "Invalid service provider id....";
			$data = [];
		}
		echo json_encode(array(
			"status"	=>	$status,
			"msg"		=>	$msg,
			"data"		=>	$data,
		));
	}

	public function identityverification(){
		$post = $this->input->post();
		$profile_id = $post['profile_id'];
		$identification_name = $post['identification_name'];
		$identification_number = $post['identification_number'];
		$identification_front_photo = $post['identification_front_photo'];
		$identification_back_photo = $post['identification_back_photo'];
		if($profile_id !='' && $identification_name !='' && $identification_number!='' && $identification_front_photo!='' && $identification_back_photo!=''){
			$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
			if($check_sp_reg){
				$chk_identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
				if(!$chk_identityverification){
					if(isset($post['identification_front_photo'])){
						$identification_front_photo = $post['identification_front_photo'];
						$identification_front_photo_id = $this->decodebase64frontimage($identification_front_photo);
					}else{
						$identification_front_photo_id =  "";
					}

					if(isset($post['identification_back_photo'])){
						$identification_back_photo = $post['identification_back_photo'];
						$identification_back_photo_id = $this->decodebase64backimage($identification_back_photo);
					}else{
						$identification_back_photo_id =  "";
					}
					if($identification_front_photo_id !='' && $identification_back_photo_id != ''){
						$identity_insert = array(
							"identification_name"			=>	$identification_name,
							"identification_number"			=>	$identification_number,
							"identification_front_photo"	=>	$identification_front_photo_id,
							"identification_back_photo"		=>	$identification_back_photo_id,
							"service_provider_id"			=>	$profile_id,
						);
						$identity_save = $this->db->insert('spidentity_verification',$identity_insert);
						if($identity_save){
							$steps_complete_array = array(
								'steps_complete' => 4,
								'complete_percentage' => 80,
							);
							$this->db->where('service_provider_id',$profile_id);
							$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
							if($update_steps){
								$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
								$profile_data = array();
								$profile_data['profile_id'] = $sp_registration_data->id;
								$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
								$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
								$profile_data['first_name'] = $sp_registration_data->first_name;
								$profile_data['middle_name'] = $sp_registration_data->middle_name;
								$profile_data['last_name'] = $sp_registration_data->last_name;
								$profile_data['email'] = $sp_registration_data->email;
								$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
								$profile_data['address'] = $sp_registration_data->address;
								$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
								$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
								$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
								if($sp_registration_data->is_declaration == 1){
									$profile_data['declaration_status'] = "Yes";
								}else{
									$profile_data['declaration_status'] = "No";
								}
								if($sp_registration_data->gender == 1){
									$profile_data['gender'] = "Male";
								}else{
									$profile_data['gender'] = "Female";
								}
								if($sp_registration_data->servicetype_id != 6){
									$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
									$profile_data['service_location'] = $sp_location->service_location;
									$profile_data['service_distance'] = $sp_location->service_distance." km";
									$profile_data['service_location'] = $sp_location->service_location;
								
									$qualification = $this->db->get_where('spqualification',array('service_provider_id'=>$profile_id))->row();
									$profile_data['qualification_name'] = $qualification->qualification_name;
									$profile_data['additional_qualification_details'] = $qualification->additional_qualification;
									$profile_data['registration_number'] = $qualification->registration_number;
									$profile_data['certification_attachment'] = $this->spprofileimage_model->showprofileimage($qualification->certification_attachment);
									$profile_data['year_of_experience'] = $qualification->year_of_experience;
								}
								$identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
								$profile_data['identification_name'] = $identityverification->identification_name;
								$profile_data['identification_number'] = $identityverification->identification_number;
								$profile_data['identification_front_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_front_photo);
								$profile_data['identification_back_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_back_photo);

								$status  = 1;
								$msg  = "Verification data inserted successfully";
								$data = $profile_data;
							}else{
								$status = 0;
								$msg ="There is some error while update the data in stepdcomplete ..";
								$data = [];
							}
						}else{
							$status = 0;
							$msg ="There is some error while insert the data in spidentityverification ...";
							$data = [];
						}
					}else{
						$status = 0;
						$msg ="Please upload the front and back photo of the document..";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "Identity verification record exits..";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Invalid Profile ID...";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "All the fields are required...";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" 	 => $msg,
			"data"	 => $data,
		));
	}

	public function profilecomplete(){
		$post = $this->input->post();
		$profile_id = $post['profile_id'];
		$is_declaration = $post['is_declaration'];
		$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
		if($check_sp_reg){
			$declaration_com = array(
				"is_declaration" => 1,
			);
			$this->db->where('id',$profile_id);
			$complete_dec= $this->db->update('serviceprovider_profile_registration',$declaration_com);
			if($complete_dec){
				$percentage_comp = array(
					'complete_percentage' => 100,
					'steps_complete' => 5,
				);
				$this->db->where('service_provider_id',$profile_id);
				$update_steps = $this->db->update("spregistration_steps",$percentage_comp);
				if($update_steps){
					$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
					$profile_data = array();
					$profile_data['profile_id'] = $sp_registration_data->id;
					$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
					$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
					$profile_data['first_name'] = $sp_registration_data->first_name;
					$profile_data['middle_name'] = $sp_registration_data->middle_name;
					$profile_data['last_name'] = $sp_registration_data->last_name;
					$profile_data['email'] = $sp_registration_data->email;
					$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
					$profile_data['address'] = $sp_registration_data->address;
					$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
					$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
					$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
					if($sp_registration_data->is_declaration == 1){
						$profile_data['declaration_status'] = "Yes";
					}else{
						$profile_data['declaration_status'] = "No";
					}
					if($sp_registration_data->gender == 1){
						$profile_data['gender'] = "Male";
					}else{
						$profile_data['gender'] = "Female";
					}
					
					$status = 1;
					$msg = "Thank you for complete the registration.";
					$data = $profile_data;
				}else{
					$status = 0;
					$msg = "There is some error while update the data in spregistration_steps ..";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "There is some error while update the data in service provider registration ..";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "Invalid service profile id..";
			$data = [];
		}

		echo json_encode(array(
			"status" => $status,
			"msg" => $msg,
			"data" => $data,
		));

	}
	
	/* ---------------------------End------------------------*/
	/*Chemist Registration*/
	
	public function chemistservicelocation(){
		$post = $this->input->post();
		$profile_id = $post['profile_id']; 
		$service_location = $post['service_location'];
		$service_distance = $post['service_distance'];
		$location_lat = $post['location_lat'];
		$location_lang = $post['location_lang'];
		$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
		if($check_sp_reg){
			$check_sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
			if(!$check_sp_location){
				$servicearea_insert = array(
					"service_location"		=> $service_location,
					"service_distance"		=> $service_distance,
					"location_lat"			=> $location_lat,
					"location_lang"			=> $location_lang,
					"service_provider_id" 	=> $profile_id,
				);
				$insert_servicelocation = $this->db->insert('spservice_location',$servicearea_insert);
				if($insert_servicelocation){
					$steps_complete_array = array(
						'steps_complete' => 2,
						'complete_percentage' => 40,
					);
					$this->db->where('service_provider_id',$profile_id);
					$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
					if($update_steps){
						$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
						$profile_data = array();
						$profile_data['profile_id'] = $sp_registration_data->id;
						$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
						$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
						$profile_data['first_name'] = $sp_registration_data->first_name;
						$profile_data['middle_name'] = $sp_registration_data->middle_name;
						$profile_data['last_name'] = $sp_registration_data->last_name;
						$profile_data['email'] = $sp_registration_data->email;
						$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
						$profile_data['address'] = $sp_registration_data->address;
						$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
						$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
						$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
						if($sp_registration_data->is_declaration == 1){
							$profile_data['declaration_status'] = "Yes";
						}else{
							$profile_data['declaration_status'] = "No";
						}
						if($sp_registration_data->gender == 1){
							$profile_data['gender'] = "Male";
						}else{
							$profile_data['gender'] = "Female";
						}
						$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
						$profile_data['service_location'] = $sp_location->service_location;
						$profile_data['service_distance'] = $sp_location->service_distance." km";
						$profile_data['service_location'] = $sp_location->service_location;
						$status  = 1;
						$msg  = "Service Location insert successfully";
						$data = $profile_data;
					}else{
						$status = 0;
						$msg = "There is some error while update the data in spregistration_steps table";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "There is some error while update the data in spservice_location table";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Record is exits in the spservice_location table..";
				$data = []; 
			}
		}else{
			$status = 0;
			$msg = "Record is not available in the serviceprovider_profile_registration..";
			$data = []; 
		}

		echo json_encode(array(
			"status" => $status,
			"msg"	 => $msg,
			"data"   => $data,
		));
	}

	public function chemist_shopinfo_add(){
		$post = $this->input->post();
		$drug_licence_no = $post['drug_licence_no'];
		$pan_no = $post['pan_no'];
		$gst_no = $post['gst_no'];
		$profile_id = $post['service_provider_id'];
		if(isset($post['drug_licence_photo']) && $post['drug_licence_photo']!=''){
			$drug_licence_photo = $this->decodebase64image($post['drug_licence_photo']);
		}else{
			$drug_licence_photo = "";
		}
		if(isset($post['pan_photo']) && $post['pan_photo']!=''){
			$pan_photo = $this->decodebase64image($post['pan_photo']);
		}else{
			$pan_photo = "";
		}
		
		if(isset($post['gst_document']) && $post['gst_document']!=''){
			$gst_document = $this->decodebase64image($post['gst_document']);
		}else{
			$gst_document = "";
		}
		if($drug_licence_photo!='' && $pan_photo!='' && $gst_document!=''){
			$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
			if($check_sp_reg){
				$check_shopinfo = $this->db->get_where('chemistshopinformation',array(
					'service_provider_id'=> $profile_id,
				))->row();
				if(!$check_shopinfo){
					$shopinfo_insert_array = array(
						"drug_licence_no" 		=>	$drug_licence_no,
						"drug_licence_photo" 	=>	$drug_licence_photo,
						"pan_no" 				=>	$pan_no,
						"pan_photo"				=>	$pan_photo,
						"gst_no" 				=>	$gst_no,
						"gst_document" 			=>	$gst_document,
						"service_provider_id" 	=>	$profile_id,
					);
					$create_shopinfo = $this->db->insert('chemistshopinformation',$shopinfo_insert_array);
					if($create_shopinfo){
						$steps_complete_array = array(
							'steps_complete' => 3,
							'complete_percentage' => 60,
						);
						$this->db->where('service_provider_id',$profile_id);
						$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
						if($update_steps){
							$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
							$profile_data = array();
							$profile_data['profile_id'] = $sp_registration_data->id;
							$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
							$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
							$profile_data['first_name'] = $sp_registration_data->first_name;
							$profile_data['middle_name'] = $sp_registration_data->middle_name;
							$profile_data['last_name'] = $sp_registration_data->last_name;
							$profile_data['email'] = $sp_registration_data->email;
							$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
							$profile_data['address'] = $sp_registration_data->address;
							$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
							$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
							$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
							if($sp_registration_data->is_declaration == 1){
								$profile_data['declaration_status'] = "Yes";
							}else{
								$profile_data['declaration_status'] = "No";
							}
							if($sp_registration_data->gender == 1){
								$profile_data['gender'] = "Male";
							}else{
								$profile_data['gender'] = "Female";
							}
							$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
							$profile_data['service_location'] = $sp_location->service_location;
							$profile_data['service_distance'] = $sp_location->service_distance." km";
							$profile_data['service_location'] = $sp_location->service_location;
							$shop_info_details = $this->db->get_where("chemistshopinformation",array('service_provider_id' => $profile_id ))->row();
							$profile_data['drug_licence_no'] = $shop_info_details->drug_licence_no;
							$profile_data['drug_licence_photo'] = $this->spprofileimage_model->showprofileimage($shop_info_details->drug_licence_photo);
							$profile_data['pan_no'] = $shop_info_details->pan_no;
							$profile_data['pan_photo'] = $this->spprofileimage_model->showprofileimage($shop_info_details->pan_photo);
							$profile_data['gst_no'] = $shop_info_details->gst_no;
							$profile_data['gst_document'] = $this->spprofileimage_model->showprofileimage($shop_info_details->gst_document);
							
							$status = 1;
							$msg = "Success";
							$data = $profile_data;

						}else{
							$status = 0;
							$msg = "There is some error while update the steps..";
							$data = [];
						}
					}else{
						$status = 	0;
						$msg 	=	"There is some error while adding";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "Shop info already exits..";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Invalid service provider id..";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "All the fields are required..";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" => $msg,
			"data" => $data,
		));		
	}
	
	public function chemist_identity_verification(){
		$post = $this->input->post();
		$profile_id = $post['service_provider_id'];
		$shop_name = $post['shop_name'];
		$shop_address_proof_type = $post['shop_address_proof_type'];
		
		if(isset($post['shop_photo']) && $post['shop_photo']!=''){
			$shop_photo = $this->decodebase64backimage($post['shop_photo']);
		}else{
			$shop_photo = "";
		}
		if(isset($post['address_proof_document']) && $post['address_proof_document']!=''){
			$address_proof_document = $this->decodebase64image($post['address_proof_document']);
		}else{
			$address_proof_document = "";
		}
		
		if($shop_photo != '' && $address_proof_document != ''){
			$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
			if($check_sp_reg){
				$check_data = $this->db->get_where("chemistidentityverification",array(
					'service_provider_id'=>$profile_id,
				))->row();
				if(!$check_data){
					$verificationdata_insert = array(
						"service_provider_id"		=>	$profile_id,
						"shop_name"					=>	$shop_name,
						"shop_photo"				=>	$shop_photo,
						"shop_address_proof_type"	=>	$shop_address_proof_type,
						"address_proof_document"	=>	$address_proof_document,
					);
					$insert = $this->db->insert("chemistidentityverification",$verificationdata_insert);
					if($insert){
						$steps_complete_array = array(
							'steps_complete' => 2,
							'complete_percentage' => 80,
						);
						$this->db->where('service_provider_id',$profile_id);
						$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
						if($update_steps){
							$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
							$profile_data = array();
							$profile_data['profile_id'] = $sp_registration_data->id;
							$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
							$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
							$profile_data['first_name'] = $sp_registration_data->first_name;
							$profile_data['middle_name'] = $sp_registration_data->middle_name;
							$profile_data['last_name'] = $sp_registration_data->last_name;
							$profile_data['email'] = $sp_registration_data->email;
							$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
							$profile_data['address'] = $sp_registration_data->address;
							$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
							$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
							$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
							if($sp_registration_data->is_declaration == 1){
								$profile_data['declaration_status'] = "Yes";
							}else{
								$profile_data['declaration_status'] = "No";
							}
							if($sp_registration_data->gender == 1){
								$profile_data['gender'] = "Male";
							}else{
								$profile_data['gender'] = "Female";
							}
							$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
							$profile_data['service_location'] = $sp_location->service_location;
							$profile_data['service_distance'] = $sp_location->service_distance." km";
							$profile_data['service_location'] = $sp_location->service_location;
							$shop_info_details = $this->db->get_where("chemistshopinformation",array('service_provider_id' => $profile_id ))->row();
							$profile_data['drug_licence_no'] = $shop_info_details->drug_licence_no;
							$profile_data['drug_licence_photo'] = $this->spprofileimage_model->showprofileimage($shop_info_details->drug_licence_photo);
							$profile_data['pan_no'] = $shop_info_details->pan_no;
							$profile_data['pan_photo'] = $this->spprofileimage_model->showprofileimage($shop_info_details->pan_photo);
							$profile_data['gst_no'] = $shop_info_details->gst_no;
							$profile_data['gst_document'] = $this->spprofileimage_model->showprofileimage($shop_info_details->gst_document);
							$chemistidentityverification_details = $this->db->get_where("chemistidentityverification",array('service_provider_id' => $profile_id ))->row();
							$profile_data['shop_name'] = $chemistidentityverification_details->shop_name;
							$profile_data['shop_photo'] = $this->spprofileimage_model->showprofileimage($chemistidentityverification_details->shop_photo);
							$profile_data['shop_address_proof_type'] = $chemistidentityverification_details->shop_address_proof_type;
							$profile_data['address_proof_document'] = $this->spprofileimage_model->showprofileimage($chemistidentityverification_details->address_proof_document);
							
							$status = 1;
							$msg = "Success";
							$data = $profile_data;
						}else{
							$status = 1;
							$msg = "There is some error while update the steps";
							$data = [];
						}
					}else{
						$status = 0;
						$msg = "There is some error while insert the data in chemistidentityverification table..";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "Indentity record already exits...";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Invalid service provider ID..";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "All the fields are required..";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" => $msg,
			"data" => $data,
		));

	}
	
	public function labinfo_register(){
		$post = $this->input->post();
		$service_provider_id = $post['service_provider_id'];
		$lab_name = $post['lab_name'];
		$lab_mobile_no = $post['lab_mobile_no'];
		$lab_email_id = $post['lab_email_id'];
		$lab_address_proof_type = $post['lab_address_proof_type'];
		$lab_lat = $post['lab_lat'];
		$lab_lang = $post['lab_lang'];

		if(isset($post['lab_profile_picture'])){
			$profile_image = 	$post['lab_profile_picture'];
			$image_id = $this->decodebase64backimage($profile_image);
		}else{
			$image_id =  "";
		}
		if(isset($post['lab_upload_document'])){
			$lab_upload_document = 	$post['lab_upload_document'];
			$lab_upload_document_id = $this->decodebase64image($lab_upload_document);
		}else{
			$lab_upload_document_id =  "";
		}
		$sp_mobile_chk = $this->db->get_where('serviceprovider_profile_registration',array('id'=>$service_provider_id,'status'=>1))->row();
		if($sp_mobile_chk){
			$check_labinfo = $this->db->get_where("labinformation",array('service_provider_id'=>$service_provider_id))->row();
			if(!$check_labinfo){
				$insert_labinfo = array(
					"service_provider_id"		=>	$service_provider_id,
					"lab_name"					=>	$lab_name,
					"lab_profile_picture"		=>	$lab_upload_document_id,
					"lab_mobile_no"				=>	$lab_mobile_no,
					"lab_email_id"				=>	$lab_email_id,
					"lab_address_proof_type"	=>	$lab_address_proof_type,
					"lab_upload_document"		=>	$lab_upload_document_id,
					"lab_lat"					=>	$lab_lat,
					"lab_lang"					=>	$lab_lang,
				);
				$labinfo_save = $this->db->insert("labinformation",$insert_labinfo);
				if($labinfo_save){
					$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$service_provider_id))->row();
					$profile_data = array();
					$profile_data['profile_id'] = $sp_registration_data->id;
					$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
					$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
					$profile_data['first_name'] = $sp_registration_data->first_name;
					$profile_data['middle_name'] = $sp_registration_data->middle_name;
					$profile_data['last_name'] = $sp_registration_data->last_name;
					$profile_data['email'] = $sp_registration_data->email;
					$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
					$profile_data['address'] = $sp_registration_data->address;
					$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
					$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
					$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
					if($sp_registration_data->is_declaration == 1){
						$profile_data['declaration_status'] = "Yes";
					}else{
						$profile_data['declaration_status'] = "No";
					}
					if($sp_registration_data->gender == 1){
						$profile_data['gender'] = "Male";
					}else{
						$profile_data['gender'] = "Female";
					}
					$lab_info_data = $this->db->get_where("labinformation",array('service_provider_id'=>$service_provider_id))->row();
					$profile_data['lab_name'] = $lab_info_data->lab_name;
					$profile_data['lab_profile_picture'] = $this->spprofileimage_model->showprofileimage($lab_info_data->lab_profile_picture);
					$profile_data['lab_mobile_no'] = $lab_info_data->lab_mobile_no;
					$profile_data['lab_email_id'] = $lab_info_data->lab_email_id;
					$profile_data['lab_address_proof_type'] = $lab_info_data->lab_address_proof_type;
					$profile_data['lab_upload_document'] = $this->spprofileimage_model->showprofileimage($lab_info_data->lab_upload_document);
					
					$status = 1;
					$msg = "Success";
					$data = $profile_data;
				}else{
					$status = 0;
					$msg = "There is some error ";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Laboratory information already exits..";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "Invalid Service Provider Profile ID...";
			$data = [];
		}
		
		echo json_encode(array(
			"status" 	=> $status,
			"msg" 		=> $msg,
			"data" 		=> $data
		));

	}
	
	public function labowner_identityverification(){
		$post = $this->input->post();
		$profile_id = $post['profile_id'];
		$identification_name = $post['identification_name'];
		$identification_number = $post['identification_number'];
		$identification_front_photo = $post['identification_front_photo'];
		$identification_back_photo = $post['identification_back_photo'];
		if($profile_id !='' && $identification_name !='' && $identification_number!='' && $identification_front_photo!='' && $identification_back_photo!=''){
			$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
			if($check_sp_reg){
				$chk_identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
				if(!$chk_identityverification){
					if(isset($post['identification_front_photo'])){
						$identification_front_photo = $post['identification_front_photo'];
						$identification_front_photo_id = $this->decodebase64frontimage($identification_front_photo);
					}else{
						$identification_front_photo_id =  "";
					}

					if(isset($post['identification_back_photo'])){
						$identification_back_photo = $post['identification_back_photo'];
						$identification_back_photo_id = $this->decodebase64backimage($identification_back_photo);
					}else{
						$identification_back_photo_id =  "";
					}
					if($identification_front_photo_id !='' && $identification_back_photo_id != ''){
						$identity_insert = array(
							"identification_name"			=>	$identification_name,
							"identification_number"			=>	$identification_number,
							"identification_front_photo"	=>	$identification_front_photo_id,
							"identification_back_photo"		=>	$identification_back_photo_id,
							"service_provider_id"			=>	$profile_id,
						);
						$identity_save = $this->db->insert('spidentity_verification',$identity_insert);
						if($identity_save){
							$steps_complete_array = array(
								'steps_complete' => 4,
								'complete_percentage' => 80,
							);
							$this->db->where('service_provider_id',$profile_id);
							$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
							if($update_steps){
								$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
								$profile_data = array();
								$profile_data['profile_id'] = $sp_registration_data->id;
								$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
								$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
								$profile_data['first_name'] = $sp_registration_data->first_name;
								$profile_data['middle_name'] = $sp_registration_data->middle_name;
								$profile_data['last_name'] = $sp_registration_data->last_name;
								$profile_data['email'] = $sp_registration_data->email;
								$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
								$profile_data['address'] = $sp_registration_data->address;
								$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
								$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
								$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
								if($sp_registration_data->is_declaration == 1){
									$profile_data['declaration_status'] = "Yes";
								}else{
									$profile_data['declaration_status'] = "No";
								}
								if($sp_registration_data->gender == 1){
									$profile_data['gender'] = "Male";
								}else{
									$profile_data['gender'] = "Female";
								}
								$lab_info_data = $this->db->get_where("labinformation",array('service_provider_id'=>$profile_id))->row();
								$profile_data['lab_name'] = $lab_info_data->lab_name;
								$profile_data['lab_profile_picture'] = $this->spprofileimage_model->showprofileimage($lab_info_data->lab_profile_picture);
								$profile_data['lab_mobile_no'] = $lab_info_data->lab_mobile_no;
								$profile_data['lab_email_id'] = $lab_info_data->lab_email_id;
								$profile_data['lab_address_proof_type'] = $lab_info_data->lab_address_proof_type;
								$profile_data['lab_upload_document'] = $this->spprofileimage_model->showprofileimage($lab_info_data->lab_upload_document);
								
								$identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
								$profile_data['identification_name'] = $identityverification->identification_name;
								$profile_data['identification_number'] = $identityverification->identification_number;
								$profile_data['identification_front_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_front_photo);
								$profile_data['identification_back_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_back_photo);

								$status  = 1;
								$msg  = "Verification data inserted successfully";
								$data = $profile_data;
							}else{
								$status = 0;
								$msg ="There is some error while update the data in stepdcomplete ..";
								$data = [];
							}
						}else{
							$status = 0;
							$msg ="There is some error while insert the data in spidentityverification ...";
							$data = [];
						}
					}else{
						$status = 0;
						$msg ="Please upload the front and back photo of the document..";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "Identity verification record exits..";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Invalid Profile ID...";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "All the fields are required...";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" 	 => $msg,
			"data"	 => $data,
		));
	}
	
	
	
	public function ambulanceservicelocation_reg(){
		$post = $this->input->post();
		$profile_id = $post['profile_id']; 
		$service_location = $post['service_location'];
		$service_distance = $post['service_distance'];
		$location_lat = $post['location_lat'];
		$location_lang = $post['location_lang'];
		$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
		if($check_sp_reg){
			$check_sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
			if(!$check_sp_location){
				$servicearea_insert = array(
					"service_location"		=> $service_location,
					"service_distance"		=> $service_distance,
					"location_lat"			=> $location_lat,
					"location_lang"			=> $location_lang,
					"service_provider_id" 	=> $profile_id,
				);
				$insert_servicelocation = $this->db->insert('spservice_location',$servicearea_insert);
				if($insert_servicelocation){
					$steps_complete_array = array(
						'steps_complete' => 2,
						'complete_percentage' => 40,
					);
					$this->db->where('service_provider_id',$profile_id);
					$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
					if($update_steps){
						$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
						$profile_data = array();
						$profile_data['profile_id'] = $sp_registration_data->id;
						$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
						$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
						$profile_data['first_name'] = $sp_registration_data->first_name;
						$profile_data['middle_name'] = $sp_registration_data->middle_name;
						$profile_data['last_name'] = $sp_registration_data->last_name;
						$profile_data['email'] = $sp_registration_data->email;
						$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
						$profile_data['address'] = $sp_registration_data->address;
						$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
						$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
						$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
						if($sp_registration_data->is_declaration == 1){
							$profile_data['declaration_status'] = "Yes";
						}else{
							$profile_data['declaration_status'] = "No";
						}
						if($sp_registration_data->gender == 1){
							$profile_data['gender'] = "Male";
						}else{
							$profile_data['gender'] = "Female";
						}
						$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
						$profile_data['service_location'] = $sp_location->service_location;
						$profile_data['service_distance'] = $sp_location->service_distance." km";
						$profile_data['service_location'] = $sp_location->service_location;
						$status  = 1;
						$msg  = "Service Location insert successfully";
						$data = $profile_data;
					}else{
						$status = 0;
						$msg = "There is some error while update the data in spregistration_steps table";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "There is some error while update the data in spservice_location table";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Record is exits in the spservice_location table..";
				$data = []; 
			}
		}else{
			$status = 0;
			$msg = "Record is not available in the serviceprovider_profile_registration..";
			$data = []; 
		}

		echo json_encode(array(
			"status" => $status,
			"msg"	 => $msg,
			"data"   => $data,
		));
	}
	
	
	public function vehicle_professional_reg(){
		$post = $this->input->post();
		$profile_id		= $post['profile_id'];
		$name 						= $post['name'];
		$driving_license_number 	= $post['driving_license_number'];
		if(isset($post['dl_photo'])){
			$dl_photo_base64 = 	$post['dl_photo'];
			$dl_photo = $this->decodebase64image($dl_photo_base64);
		}else{
			$dl_photo = "";
		}
		if($dl_photo !=''){
			$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
			if($check_sp_reg){
				$check_professional_reg = $this->db->get_where("ambulancedriverprofessional",array('service_provider_id' => $profile_id ))->row();
				if(!$check_professional_reg){
					$insert = array(
						"service_provider_id" 		=>	$profile_id,
						"name" 						=>	$name,
						"driving_license_number" 	=>	$driving_license_number,
						"dl_photo" 					=>	$dl_photo,
					);
					$insert_professional_qualification = $this->db->insert("ambulancedriverprofessional",$insert);
					if($insert_professional_qualification){
						
						$steps_complete_array = array(
							'steps_complete' => 3,
							'complete_percentage' => 60,
						);
						$this->db->where('service_provider_id',$profile_id);
						$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
						if(	$update_steps){
						$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
						$profile_data = array();
						$profile_data['profile_id'] = $sp_registration_data->id;
						$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
						$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
						$profile_data['first_name'] = $sp_registration_data->first_name;
						$profile_data['middle_name'] = $sp_registration_data->middle_name;
						$profile_data['last_name'] = $sp_registration_data->last_name;
						$profile_data['email'] = $sp_registration_data->email;
						$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
						$profile_data['address'] = $sp_registration_data->address;
						$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
						$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
						$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
						if($sp_registration_data->is_declaration == 1){
							$profile_data['declaration_status'] = "Yes";
						}else{
							$profile_data['declaration_status'] = "No";
						}
						if($sp_registration_data->gender == 1){
							$profile_data['gender'] = "Male";
						}else{
							$profile_data['gender'] = "Female";
						}
						$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
						$profile_data['service_location'] = $sp_location->service_location;
						$profile_data['service_distance'] = $sp_location->service_distance." km";
						$profile_data['service_location'] = $sp_location->service_location;
						
						$professional_data = $this->db->get_where("ambulancedriverprofessional",array('service_provider_id'=>$profile_id))->row();
						$profile_data['adp_name'] = $professional_data->name;
						$profile_data['adp_dl_no'] = $professional_data->driving_license_number;
						$profile_data['adp_dl_photo'] = $this->spprofileimage_model->showprofileimage($professional_data->dl_photo);
						
						$status =1;
						$msg = "Professional qualification insert successfully";
						$data = $profile_data;
						
						}else{
							$status = 0;
							$msg = "There is some error while update the step percentage..";
							$data = [];
						}
					}else{
						$status = 0;
						$msg = "There is some error while insert the record..";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "Duplicate record entry for ambulancedriverprofessional table";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Please complete the service profile registration..";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "Please upload the DL Photo";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" 	 => $msg,
			"data"	 => $data,
		));
	}
	
	
	public function decodebase64frontimage($image){
		if($image!=""){
			$output_file = 'upload/';
			$output_file_with_extension = time().".png";
			file_put_contents( $output_file . $output_file_with_extension, base64_decode($image) );
			$file_path = base_url();
			$file_save_path = $file_path.$output_file.$output_file_with_extension;
			$profile_image_insert = array(
				"profile_image" => $file_save_path,
			);
			$image_insert = $this->db->insert("sp_profile_image",$profile_image_insert);
			if($image_insert){
				$image_id = $this->db->insert_id();
			}else{
				$image_id = "";
			}
		}else{
			$image_id ="";
		}
		return $image_id;
	}
	public function decodebase64backimage($image){
		if($image!=""){
			$output_file = 'upload/';
			$output_file_with_extension = rand()."".time().".png";
			file_put_contents( $output_file . $output_file_with_extension, base64_decode($image) );
			$file_path = base_url();
			$file_save_path = $file_path.$output_file.$output_file_with_extension;
			$profile_image_insert = array(
				"profile_image" => $file_save_path,
			);
			$image_insert = $this->db->insert("sp_profile_image",$profile_image_insert);
			if($image_insert){
				$image_id = $this->db->insert_id();
			}else{
				$image_id = "";
			}
		}else{
			$image_id ="";
		}
		return $image_id;
	}
	
	public function amb_dri_iden_verification(){
		$post = $this->input->post();
		$profile_id = $post['profile_id'];
		$identification_name = $post['identification_name'];
		$identification_number = $post['identification_number'];
		$identification_front_photo = $post['identification_front_photo'];
		$identification_back_photo = $post['identification_back_photo'];
		
		
		if($profile_id !='' && $identification_name !='' && $identification_number!='' && $identification_front_photo!='' && $identification_back_photo!=''){
			$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
			if($check_sp_reg){
				$chk_identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
				if(!$chk_identityverification){
				
					$identification_front_photo_id = $this->decodebase64frontimage($identification_front_photo);
					$identification_back_photo_id = $this->decodebase64backimage($identification_back_photo);
					if($identification_front_photo_id !=''){
						$identity_insert = array(
							"identification_name"			=>	$identification_name,
							"identification_number"			=>	$identification_number,
							"identification_front_photo"	=>	$identification_front_photo_id,
							"identification_back_photo"		=>	$identification_back_photo_id,
							"service_provider_id"			=>	$profile_id,
						);
						$identity_save = $this->db->insert('spidentity_verification',$identity_insert);
						if($identity_save){
							
							$steps_complete_array = array(
								'steps_complete' => 4,
								'complete_percentage' => 80,
							);
							$this->db->where('service_provider_id',$profile_id);
							$update_steps = $this->db->update("spregistration_steps",$steps_complete_array);
							if($update_steps){
								$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
								$profile_data = array();
								$profile_data['profile_id'] = $sp_registration_data->id;
								$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
								$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
								$profile_data['first_name'] = $sp_registration_data->first_name;
								$profile_data['middle_name'] = $sp_registration_data->middle_name;
								$profile_data['last_name'] = $sp_registration_data->last_name;
								$profile_data['email'] = $sp_registration_data->email;
								$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
								$profile_data['address'] = $sp_registration_data->address;
								$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
								$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
								$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
								if($sp_registration_data->is_declaration == 1){
									$profile_data['declaration_status'] = "Yes";
								}else{
									$profile_data['declaration_status'] = "No";
								}
								if($sp_registration_data->gender == 1){
									$profile_data['gender'] = "Male";
								}else{
									$profile_data['gender'] = "Female";
								}
								$sp_location = $this->db->get_where('spservice_location',array('service_provider_id'=>$profile_id,'status'=>1))->row();
								$profile_data['service_location'] = $sp_location->service_location;
								$profile_data['service_distance'] = $sp_location->service_distance." km";
								$profile_data['service_location'] = $sp_location->service_location;
								
								$professional_data = $this->db->get_where("ambulancedriverprofessional",array('service_provider_id'=>$profile_id))->row();
								$profile_data['adp_name'] = $professional_data->name;
								$profile_data['adp_dl_no'] = $professional_data->driving_license_number;
								$profile_data['adp_dl_photo'] = $this->spprofileimage_model->showprofileimage($professional_data->dl_photo);
								
								$identityverification = $this->db->get_where('spidentity_verification',array('service_provider_id'=>$profile_id))->row();
								$profile_data['identification_name'] = $identityverification->identification_name;
								$profile_data['identification_number'] = $identityverification->identification_number;
								$profile_data['identification_front_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_front_photo);
								$profile_data['identification_back_photo'] = $this->spprofileimage_model->showprofileimage($identityverification->identification_back_photo);

								$status  = 1;
								$msg  = "Verification data inserted successfully";
								$data = $profile_data;
							}else{
								$status = 0;
								$msg ="There is some error while update the data in stepdcomplete ..";
								$data = [];
							}
							
						}else{
							$status = 0;
							$msg ="There is some error while insert the data in spidentityverification ...";
							$data = [];
						}
					}else{
						$status = 0;
						$msg ="Please upload the front and back photo of the document..";
						$data = [];
					}
				}else{
					$status = 0;
					$msg = "Identity verification record exits..";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Invalid Profile ID...";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "All the fields are required...";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" 	 => $msg,
			"data"	 => $data,
		));
	}
	
	
	public function addvehicleinfo(){
		$post = $this->input->post();
		$profile_id = $post['service_provider_id'];
		$vehicle_json = json_decode($post['vehicle_details']);
		
		
		$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
		if($check_sp_reg){
			foreach($vehicle_json as $key=>$value){
				$insert = array(
					"service_provider_id"	=>	$profile_id,
					"vehicle_name"			=>	$value->vehicle_name,
					"vehicle_type"			=>	$value->vehicle_type,
					"registration_date"		=>	date("Y-m-d",strtotime($value->registration_date)),
					"vehicle_reg_no"		=>	$value->vehicle_reg_no,
					"reg_certificate_photo"	=>	$this->decodebase64image($value->reg_certificate_photo),
				);
				$ambvehicledetails_insert = $this->db->insert("ambvehicledetails",$insert);
			}
			$percentage_comp = array(
				'complete_percentage' => 60,
				'steps_complete' => 3,
			);
			$this->db->where('service_provider_id',$profile_id);
			$update_steps = $this->db->update("spregistration_steps",$percentage_comp);
			if($update_steps){
				$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
				$profile_data = array();
				$profile_data['profile_id'] = $sp_registration_data->id;
				$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
				$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
				$profile_data['first_name'] = $sp_registration_data->first_name;
				$profile_data['middle_name'] = $sp_registration_data->middle_name;
				$profile_data['last_name'] = $sp_registration_data->last_name;
				$profile_data['email'] = $sp_registration_data->email;
				$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
				$profile_data['address'] = $sp_registration_data->address;
				$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
				$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
				$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
				if($sp_registration_data->is_declaration == 1){
					$profile_data['declaration_status'] = "Yes";
				}else{
					$profile_data['declaration_status'] = "No";
				}
				if($sp_registration_data->gender == 1){
					$profile_data['gender'] = "Male";
				}else{
					$profile_data['gender'] = "Female";
				}
				$vehicle_info = $this->db->query("Select id,vehicle_type,vehicle_reg_no,reg_certificate_photo,vehicle_name from ambvehicledetails where service_provider_id='$profile_id'")->result_array();
				if(count($vehicle_info)>0){
					$profile_data['vehicle_info'] = $vehicle_info;
				}else{
					$profile_data['vehicle_info'] = [];
				}
				$status = 1;
				$msg = "Ambulance vehicle added successfully";
				$data = $profile_data;
			}else{
				$status = 0;
				$msg = "There is some error while update the registration steps..";
				$data = [];
			}
			
		}else{
			$status = 0;
			$msg = "Profile data not available..";
			$data = [];
		}
		echo json_encode(array(
			"status" 	=> $status,
			"msg" 		=> $msg,
			"data" 		=> $data
		));
	}
	
	public function driver_info(){
		$post = $this->input->post();
		$profile_id 			= $post['service_provider_id'];
		$driver_info = json_decode($post['driver_info']);
		$check_sp_reg = $this->db->get_where("serviceprovider_profile_registration",array('id' =>$profile_id,'status'=>1))->row();
		if($check_sp_reg){
			$check_vehicle = $this->db->get_where("ambdrivervehiclemaping",array('service_provider_id'=>$profile_id))->row();
			if(!$check_vehicle){
				foreach($driver_info as $key => $value ){
					$vehicle_driver_map = array(
						"service_provider_id"	=>	$profile_id,
						"ambvehicledetails_id"	=>	$value->ambvehicledetails_id,
						"ambulance_driver_id"	=>	$value->ambulance_driver_id,
					);
					$mapping = $this->db->insert('ambdrivervehiclemaping',$vehicle_driver_map);
				}
				$percent = $this->db->get_where("spregistration_steps",array('service_provider_id'=>$profile_id))->row();
				if($percent){
					$percentage_comp = array(
						'complete_percentage' => 80,
						'steps_complete' => 4,
					);
					$this->db->where('service_provider_id',$profile_id);
					$update_steps = $this->db->update("spregistration_steps",$percentage_comp);
					if($update_steps){
						$sp_registration_data = $this->db->get_where("serviceprovider_profile_registration",array('id'=>$profile_id))->row();
						$profile_data = array();
						$profile_data['profile_id'] = $sp_registration_data->id;
						$profile_data['service_name'] = $this->serviceslist_model->service_name($sp_registration_data->servicetype_id);
						$profile_data['mobile_no'] = $sp_registration_data->mobile_no;
						$profile_data['first_name'] = $sp_registration_data->first_name;
						$profile_data['middle_name'] = $sp_registration_data->middle_name;
						$profile_data['last_name'] = $sp_registration_data->last_name;
						$profile_data['email'] = $sp_registration_data->email;
						$profile_data['date_of_birth'] = date("d-m-Y",strtotime($sp_registration_data->date_of_birth));
						$profile_data['address'] = $sp_registration_data->address;
						$profile_data['profileimage'] = $this->spprofileimage_model->showprofileimage($sp_registration_data->profileimage_id);
						$profile_data['complete_percentage'] = $this->spprofileimage_model->getcompletepercentage($sp_registration_data->id);
						$profile_data['verification_status'] = $this->spprofileimage_model->isverified($sp_registration_data->id);
						if($sp_registration_data->is_declaration == 1){
							$profile_data['declaration_status'] = "Yes";
						}else{
							$profile_data['declaration_status'] = "No";
						}
						if($sp_registration_data->gender == 1){
							$profile_data['gender'] = "Male";
						}else{
							$profile_data['gender'] = "Female";
						}
						$vehicle_info = $this->db->query("Select id,vehicle_type,registration_date,vehicle_reg_no,reg_certificate_photo,vehicle_name from ambvehicledetails where service_provider_id='$profile_id'")->result_array();
						if(count($vehicle_info)>0){
							$profile_data['vehicle_info'] = $vehicle_info;
						}else{
							$profile_data['vehicle_info'] = [];
						}
						$mapping_info = $this->db->get_where("ambdrivervehiclemaping",array('service_provider_id'=>$profile_id))->result();
						$mapp = array();
						if($mapping_info){
							foreach($mapping_info as $key=>$value){
								$mapp[$key]['ambvehicledetails_id'] = $value->ambvehicledetails_id;
								$mapp[$key]['ambulance_driver_id'] = $value->ambulance_driver_id;
								$vehicle_details = $this->db->get_where('ambvehicledetails',array('id'=>$value->ambvehicledetails_id))->row();
								$mapp[$key]['ambvehicles_name'] = $vehicle_details->vehicle_name;
								$driver_det = $this->db->get_where('ambulancedriverprofessional',array('service_provider_id'=>$value->ambulance_driver_id))->row();
								$mapp[$key]['ambdriver_name'] = $driver_det->name;
							}
						}else{
							$mapp = [];
						}
						$profile_data['vehicle_driver_info'] = $mapp;
						$status = 1;
						$msg = "Vehicel drive data save successfully. Please go to the declaration part..";
						$data = $profile_data;
						
					}
				}else{
					$status = 0;
					$msg = "There is some error while find the data in percentage table..";
					$data = [];
				}
				
			}else{
				$status = 0;
				$msg = "Duplicate record entry for vehicle driver...";
				$data = [];
			}
			
		}else{
			$status = 0;
			$msg = "Please complete the profile registration part...";
			$data = [];
		}
		echo json_encode(array(
			"status" 	=> $status,
			"msg" 		=> $msg,
			"data" 		=> $data
		));
	}
	
	
	public function send_request(){
		$post = $this->input->post();
		$profile_id = $post['service_provider_id'];
		if($profile_id){
			$check_request = $this->db->get_where("spregistration_steps",array('service_provider_id'=>$profile_id))->row();
			if($check_request){
				if($check_request->isverified == 0){
					$this->db->where('service_provider_id',$profile_id);
					$update_qry = $this->db->update('spregistration_steps',array('isverified'=>1));
					if($update_qry){
						$status = 1;
						$msg = "Request send successfully. Please wait for verification";
					}else{
						$status = 0;
						$msg = "There is some error while sending the request..";
					}
				}else if($check_request->isverified == 1){
					$status = 0;
					$msg = "Already request send...";
				}else{
					$status = 0;
					$msg = "There is some error.Please try after sometimes..";
				}
			}else{
				$status = 0;
				$msg = "There is no data available in the steps complete table.";
			}
		}else{
			$status = 0;
			$msg = "Invalid Service Provider ID...";
		}
		echo json_encode(array(
			"status" => $status,
			"msg" => $msg,
		));
	}
	
	public function getAmbulancedriverlist(){
		$list = $this->db->get_where("serviceprovider_profile_registration",array('status'=>1,'servicetype_id'=>7))->result();
		$free_amb_dri = array();
		if($list){
			$i = 0;
			foreach($list as $key=>$value){
				$chk_complete_percentage = $this->db->get_where("spregistration_steps",array('service_provider_id'=>$value->id))->row();
				if($chk_complete_percentage){
					if($chk_complete_percentage->complete_percentage == 100){				
						$free_amb_dri[$i]['driver_id'] = $value->id;
						$dl_details = $this->db->get_where("ambulancedriverprofessional",array('service_provider_id'=>$value->id))->row_array();
						$free_amb_dri[$i]['driving_license_number'] = $dl_details['driving_license_number'];
						$free_amb_dri[$i]['driver_dl_name'] = $dl_details['name'];
						$profile_details = $this->db->get_where('serviceprovider_profile_registration',array('id'=>$value->id))->row();
						$free_amb_dri[$i]['driver_phone_no'] = $profile_details->mobile_no;
						$i++;
					}
				}else{
					$free_amb_dri = [];
				}
			}
		}else{
			$free_amb_dri = [];
		}
		

		if(count($free_amb_dri)>0){
			$status = 1;
			$msg = "Record Available";
			$data = $free_amb_dri;
		}else{
			$status = 0;
			$msg = "Record not available";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" => $msg,
			"data" => $data,
		));
	}
	public function getAmbulancelist(){
		$post = $this->input->post();
		$profile_id = $post['service_provider_id'];
		$list = $this->db->get_where("ambvehicledetails",array('status'=>1,'service_provider_id'=>$profile_id))->result();
		$free_amb_dri = array();
		if($list){
			$i = 0;
			foreach($list as $key=>$value){
				$free_amb_dri[$i]['vehicle_id'] = $value->id;
				$free_amb_dri[$i]['vehicle_name'] = $value->vehicle_name;
				$free_amb_dri[$i]['vehicle_type'] = $value->vehicle_type;
				$free_amb_dri[$i]['vehicle_reg_no'] = $value->vehicle_reg_no;
				$i++;
			}
		}else{
			$free_amb_dri = [];
		}
		
		if(count($free_amb_dri)>0){
			$status = 1;
			$msg = "Record Available";
			$data = $free_amb_dri;
		}else{
			$status = 0;
			$msg = "Record not available";
			$data = [];
		}
		echo json_encode(array(
			"status" => $status,
			"msg" => $msg,
			"data" => $data,
		));
	}
	
	public function verificationstatus(){
		$post = $this->input->post();
		$service_provider_id = $post['service_provider_id'];
		$sp_details = $this->db->get_where("spregistration_steps",array('service_provider_id'=>$service_provider_id))->row();
		$response = array();
		if($sp_details){
			if($sp_details->isverified == 0){
				$verification_status = "Request Not Send";
				$facetofaceverification = "";
				$facetofaceaddress = "";
				$long_msg = "";
				$complete_percentage = $sp_details->complete_percentage;
			}else if($sp_details->isverified == 1){
				$verification_status = "Request Send";
				$facetofaceverification = "";
				$facetofaceaddress = "";
				$long_msg = "";
				$complete_percentage = $sp_details->complete_percentage;
			}else if($sp_details->isverified == 2){
				$verification_status = "Waiting For Physical Verification";
				$facetofaceverification = $sp_details->facetofaceverification;
				$facetofaceaddress = $sp_details->address_facetoface;
				$date = date("d-M-y H:i:s",strtotime($sp_details->facetofaceverification));
				$verficationaddress = $sp_details->address_facetoface;
				$long_msg = "Congratulation!!!Your profile has been accepted and requested to come on ".$date." at ".$verficationaddress."  for physicalverification , training & kit collection. Kindly update your Bank detailsfrom Menu section.";
				$complete_percentage = $sp_details->complete_percentage;
			}else if($sp_details->isverified == 3){
				$verification_status = "Verification Completed";
				$facetofaceverification = $sp_details->facetofaceverification;
				$facetofaceaddress = $sp_details->address_facetoface;
				$long_msg = "";
				$complete_percentage = $sp_details->complete_percentage;
			}
			
			$status = 1;
			$msg = "Success";
			$data = array(
				"verification_status" 		=> $verification_status,
				"facetofaceverification" 	=> $facetofaceverification,
				"facetofaceaddress" 		=> $facetofaceaddress,
				"long_msg" 					=> $long_msg,
				"complete_percentage" 		=> $complete_percentage,
			);
			
		}else{
			$status = 0;
			$msg = "Invalid Service provider ID";
			$data = [];
		}
		echo json_encode(array(
			"status"	=>	$status,
			"msg"	=>	$msg,
			"data"	=>	$data,
		));
	}
	
	public function vehicledriverlists(){
		$post = $this->input->post();
		$service_provider_id = $post['service_provider_id'];
		$lists = $this->db->get_where("ambdrivervehiclemaping",array('service_provider_id'=>$service_provider_id))->result();
		$list_array = array();
		
		if($lists){
			foreach($lists as $key=>$value){
				$list_array[$key]['map_id'] = $value->id;
				$list_array[$key]['ambulance_id'] = $value->ambvehicledetails_id;
				$ambulance_details = $this->db->get_where("ambvehicledetails",array('id'=>$value->ambvehicledetails_id))->row();
				$list_array[$key]['ambulance_vehicle_name'] = $ambulance_details->vehicle_name;
				$list_array[$key]['ambulance_vehicle_type'] = $ambulance_details->vehicle_type;
				$list_array[$key]['ambulance_vehicle_reg_no'] = $ambulance_details->vehicle_reg_no;
				
				$driver_details = $this->db->get_where("ambulancedriverprofessional",array('service_provider_id'=>$value->ambulance_driver_id))->row();
				$list_array[$key]['driver_id'] = $value->ambulance_driver_id;
				$list_array[$key]['driver_name'] = $driver_details->name;
				$list_array[$key]['driving_license_number'] = $driver_details->driving_license_number;
				
			}
			$data = $list_array;
			$status = 1;
			$msg = "Record Available...";
		}else{
			$data = [];
			$status = 0;
			$msg = "Record not available...";
		}
		
		echo json_encode(array(
			"status"=>$status,
			"msg"=>$msg,
			"data"=>$data
		));
	}
	
	public function spbankdetails_insert(){
		$post = $this->input->post();
		$service_provider_id = $post['service_provider_id'];
		$sp_name = $post['name'];
		$account_number = $post['account_number'];
		$ifsc_code = $post['ifsc_code'];
		$cancel_cheque_photo = $post['cancel_cheque_photo'];
		if($service_provider_id!='' && $sp_name!='' && $account_number!='' && $ifsc_code!='' && $cancel_cheque_photo!=''){
			$check_bank_details = $this->db->get_where("spbankdetails",array('service_provider_id'=>$service_provider_id,'status'=>1))->row();
			If(!$check_bank_details){
				$insert = array(
					"service_provider_id" => $service_provider_id,
					"sp_name"			  => $sp_name,
					"account_number"	  => $account_number,
					"ifsc_code"	  		  => $ifsc_code,
					"cancel_cheque_photo" => $this->decodebase64frontimage($cancel_cheque_photo),
				);
				$save_bank_details = $this->db->insert("spbankdetails",$insert);
				if($save_bank_details){
					$details = $check_bank_details = $this->db->get_where("spbankdetails",array('service_provider_id'=>$service_provider_id,'status'=>1))->row();
					$result['service_provider_id']= $details->service_provider_id;
					$result['name']= $details->sp_name;
					$result['account_number']= $details->account_number;
					$result['ifsc_code']= $details->ifsc_code;
					$result['cancel_cheque_photo']= $this->spprofileimage_model->showprofileimage($details->cancel_cheque_photo);
					$status = 1 ;
					$msg = "Bank details saved successfully";
					$data = $result;
				}else{
					$status = 0 ;
					$msg = "There is some error while saving the bank details..";
					$data = [];
				}
			}else{
				$status = 0;
				$msg = "Record already exits";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "All fields are required..";
			$data = [];
		}
		
		echo json_encode(array(	
			"status"	=>	$status,
			"msg"		=>	$msg,
			"bank_data"	=>	$data,
		));
		
	}
	
	public function sppancard_save(){
		$post = $this->input->post();
		$service_provider_id	= $post['service_provider_id'];
		$pancard_number 		= $post['pancard_number'];
		$sp_name 				= $post['name'];
		$gst_number 			= $post['gst_number'];
		$gst_document_photo 	= $post['gst_document_photo'];
		if($service_provider_id !='' && $pancard_number !='' && $sp_name != '' && $gst_number!='' && $gst_document_photo!=''){
			$check_gst = $this->db->get_where("sppancard",array('service_provider_id'=>$service_provider_id,'status'=>1))->row();
			if(!$check_gst){
				$insert = array(
					"service_provider_id" 	=> $service_provider_id,
					"pancard_number" 		=> $pancard_number,
					"sp_name"				=> $sp_name,
					"gst_number"			=> $gst_number,
					"gst_document_photo"	=> $this->decodebase64frontimage($gst_document_photo),
				);
				
				$insert_sppancard =  $this->db->insert("sppancard",$insert);
				if($insert_sppancard){
					$result = array();
					$details =	$this->db->get_where("sppancard",array('service_provider_id'=>$service_provider_id,'status'=>1))->row();
					$result['id']= $details->id;
					$result['service_provider_id']= $details->service_provider_id;
					$result['pancard_number']= $details->pancard_number;
					$result['name']= $details->sp_name;
					$result['gst_number']= $details->gst_number;
					$result['gst_document_photo']= $this->spprofileimage_model->showprofileimage($details->gst_document_photo);
					$status = 1 ;
					$msg = "Pancard details saved successfully";
					$data = $result;
				}else{
					$status = 0;
					$msg 	= "There is some error while saving the data.."; 
					$data 	= [];
				}
				
			}else{
				$status = 0;
				$msg = "Record already exits..";
				$data = [];
			}
		}else{
			$status = 0;
			$msg = "There is some error while insert the data..";
			$data = [];
		}
		
		echo json_encode(array(	
			"status"	=>	$status,
			"msg"		=>	$msg,
			"pancard"	=>	$data,
		));
		
	}
	
	
	
	
}
?>
