<?php require_once("path.php"); ?>
<script src="<?=$assets?>/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='http://localhost/vkareu/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="<?=$assets?>/js/bootstrap.min.js"></script>
<script src="<?=$assets?>/js/jquery.dataTables.min.js"></script>
<script src="<?=$assets?>/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?=$assets?>/js/dataTables.buttons.min.js"></script>
<script src="<?=$assets?>/js/buttons.flash.min.js"></script>
<script src="<?=$assets?>/js/buttons.html5.min.js"></script>
<script src="<?=$assets?>/js/buttons.print.min.js"></script>
<script src="<?=$assets?>/js/buttons.colVis.min.js"></script>
<script src="<?=$assets?>/js/dataTables.select.min.js"></script>
<script src="<?=$assets?>/js/jquery-ui.custom.min.js"></script>
<script src="<?=$assets?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?=$assets?>/js/jquery.easypiechart.min.js"></script>
<script src="<?=$assets?>/js/jquery.sparkline.index.min.js"></script>
<script src="<?=$assets?>/js/jquery.flot.min.js"></script>
<script src="<?=$assets?>/js/jquery.flot.pie.min.js"></script>
<script src="<?=$assets?>/js/jquery.flot.resize.min.js"></script>
<!-- ace scripts -->
<script src="<?=$assets?>/js/ace-elements.min.js"></script>
<script src="<?=$assets?>/js/ace.min.js"></script>
<script src="<?=$assets?>/js/bootstrap-datepicker.min.js"></script>
<script src="<?=$assets?>/js/bootstrap-timepicker.min.js"></script>
<script src="<?=$assets?>/js/moment.min.js"></script>
<script src="<?=$assets?>/js/daterangepicker.min.js"></script>
<script src="<?=$assets?>/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?=$assets?>/js/zoomify.js"></script>
<script src="<?=$assets?>/js/alertify.min.js"></script>



<script>
	$(document).ready(function(){
		/** add active class and stay opened when selected */
		var url = window.location;
		// for sidebar menu entirely but not cover treeview
		$('ul.nav-list a').filter(function() {
			 return this.href == url;
		}).parent().addClass('active');

		// for treeview
		$('ul.submenu a').filter(function() {
			 return this.href == url;
		}).parentsUntil(".nav-list > .dropdown-toggle").addClass('active open');
	});
	$('img').zoomify({
		duration: 200,
		easing:   'linear',
		scale:    0.9
	});
</script>





