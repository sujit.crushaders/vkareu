<?php 
	require_once("path.php");
	//Load Model
	$this->load->model('spprofileimage_model');
	
?>
<!DOCTYPE html>
<html lang="en">
	
	<head>
		<?php include('include/header_meta.php');?>
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">
						<small>
							<i class="fa fa-heartbeat"></i>
							Vkareu Admin
						</small>
					</a>
				</div>
				<?php include('include/header_menu.php');?>
			</div><!-- /.navbar-container -->
		</div>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>
					</div>
					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div>
				<?php include('include/left_menu.php');?>
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li>
								<a href="#">Service Providers</a>
							</li>
						</ul>
					</div>

					<div class="page-content">
						<?php include('include/theme.php');?>
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-12">
										
										<h3 class="header smaller lighter blue">Service Providers</h3>
										<?php if(!empty($msg)){ ?>
										  <div class="alert alert-success alert-dismissible" role="alert">
											  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<?php echo $msg; ?>
										  </div>
										<?php } ?>
										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										<div class="table-header">
											Results 
										</div>
										<div>
											<table id="dynamic-table" class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>#</th>
														<th>Profile Image</th>
														<th>Services<br/>Type</th>
														<th>First Name</th>
														<th>Middle Name</th>
														<th>Last Name</th>
														<th>Mobile No.</th>
														
														<th>Complete(%)</th>
														<th>Agency <br/>Req. Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php
													if($sp_list){
													foreach($sp_list as $key=> $value){
													?>
													<tr>
														<td><?=($key+1)?></td>
														<td>
															<img src="<?=$this->spprofileimage_model->showprofileimage($value->profileimage_id)?>" style="width:80px; height:80px;"/>
														</td>
														<td>
															<?=$this->spprofileimage_model->getServicetype($value->servicetype_id)?>
														</td>
														<td><?=$value->first_name?></td>
														<td><?=$value->middle_name?></td>
														<td><?=$value->last_name?></td>
														<td><?=$value->mobile_no?></td>
														<td>
															<?=$this->spprofileimage_model->getcompletepercentage($value->id)?>
														</td>
														<td>
															<?=$this->spprofileimage_model->getagencystatus($value->id)?>
														</td>
														<td>
															<a href="<?=base_url();?>dashboard/serviceproviders_details/<?=$value->id?>">
																<button class="btn btn-success">Details</button>
															</a>
														</td>
													</tr>
													<?php 
													}
													}else{
													?>
													<tr>
														<td colspan="10" style="text-align:center;">
															Record Not Available..
														</td>
													</tr>
													<?php 
													}
													?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php include('include/footer.php')?>
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
			
			
		</div><!-- /.main-container -->
		
		<?php include('include/footer_js.php')?>
		
	</body>
</html>
