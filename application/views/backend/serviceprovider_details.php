<?php 
	require_once("path.php");
	$this->load->model('spprofileimage_model');
	$status = $this->spprofileimage_model->agencyStatus($result['profile_reg']['profile_id']);
	if($status > 1){
		$approve_btn_status= "disabled";
		$cancel_btn_status= "disabled";
	}else{
		$approve_btn_status = "";
		$cancel_btn_status = "";
	}
	
	
?>
<!DOCTYPE html>
<html lang="en">
	
	<head>
		<?php include('include/header_meta.php');?>
	</head>

	<body class="no-skin">
		<div class="loading" style="display:none;"></div>
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">
						<small>
							<i class="fa fa-heartbeat"></i>
							Vkareu Admin
						</small>
					</a>
				</div>
				<?php include('include/header_menu.php');?>
			</div><!-- /.navbar-container -->
		</div>
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-signal"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-pencil"></i>
						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</button>
					</div>
					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div>
				<?php include('include/left_menu.php');?>
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li>
								<a href="#">Service Providers</a>
							</li>
						</ul>
					</div>

					<div class="page-content">
						<?php include('include/theme.php');?>
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<h3 class="header smaller lighter blue">Service Providers Details</h3>
								<div class="hr dotted"></div>
								<div>
									<div id="user-profile-2" class="user-profile">
										<div class="tabbable">
											<ul class="nav nav-tabs padding-18">
												<?php 
												if($result['profile_reg']['servicetype_id'] == 7){
												?>
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#feed">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Service Location
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#dri_pro_info">
														<i class="blue ace-icon fa fa-users bigger-120"></i>
														Driver Professional Qualification
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#pictures">
														<i class="pink ace-icon fa fa-picture-o bigger-120"></i>
														Identity Details
													</a>
												</li>
												<?php 
												}else if($result['profile_reg']['servicetype_id'] == 6){
												?>
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#vehicle_details">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Vehicle Details
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#dri_pro_qua">
														<i class="blue ace-icon fa fa-users bigger-120"></i>
														Vehicle Driver Details
													</a>
												</li>
												
												<?php
												}else if($result['profile_reg']['servicetype_id'] == 8){
												?>
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#lab_info">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Lab Information
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#friends">
														<i class="blue ace-icon fa fa-users bigger-120"></i>
														Qualification
													</a>
												</li>
												<?php
												}else if($result['profile_reg']['servicetype_id'] == 5){
												?>
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#feed">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Service Location
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#chemist">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Chemist Shop Info
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#friends">
														<i class="blue ace-icon fa fa-users bigger-120"></i>
														Qualification
													</a>
												</li>
												<?php
												}else if($result['profile_reg']['servicetype_id'] == 1 || $result['profile_reg']['servicetype_id'] == 2 || $result['profile_reg']['servicetype_id'] == 3 || $result['profile_reg']['servicetype_id'] == 4 || $result['profile_reg']['servicetype_id'] == 9){
												?>
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#feed">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Service Location
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#friends">
														<i class="blue ace-icon fa fa-users bigger-120"></i>
														Qualification
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#pictures">
														<i class="pink ace-icon fa fa-picture-o bigger-120"></i>
														Identity Details
													</a>
												</li>
												<?php
												}else{
												?>
												<li class="active">
													<a data-toggle="tab" href="#home">
														<i class="green ace-icon fa fa-user bigger-120"></i>
														Profile
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#labinfo">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Lab Information
													</a>
												</li>
												<li>
													<a data-toggle="tab" href="#vehicle_info">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Vehicle Info
													</a>
												</li>
												
												<li>
													<a data-toggle="tab" href="#vehicle_driver_info">
														<i class="orange ace-icon fa fa-rss bigger-120"></i>
														Vehicle Driver Info
													</a>
												</li>
												
												<li>
													<a data-toggle="tab" href="#friends">
														<i class="blue ace-icon fa fa-users bigger-120"></i>
														Qualification
													</a>
												</li>
												<?php 
												}
												?>
												
												
											</ul>

											<div class="tab-content no-border padding-24">
												<div id="home" class="tab-pane in active">
													<div class="row">
														<div class="col-xs-12 col-sm-3 center">
															<span class="profile-picture">
																<img class="editable img-responsive" alt="Alex's Avatar" id="avatar2" src="<?=$result['profile_reg']['profileimage'];?>" />
															</span>
															<div class="space space-4"></div>
														</div><!-- /.col -->

														<div class="col-xs-12 col-sm-9">
															<h4 class="blue">
																<span class="middle">
																	<?=$result['profile_reg']['first_name']." ".$result['profile_reg']['middle_name']." ".$result['profile_reg']['last_name'];?>	
																</span>
															</h4>
															<p><?=$result['profile_reg']['service_name'];?></p>

															<div class="profile-user-info">
																<div class="profile-info-row">
																	<div class="profile-info-name"> First Name </div>
																	<div class="profile-info-value">
																		<span><?=$result['profile_reg']['first_name']?></span>
																	</div>
																</div>
																<div class="profile-info-row">
																	<div class="profile-info-name"> Middle Name </div>
																	<div class="profile-info-value">
																		<span><?=$result['profile_reg']['middle_name']?></span>
																	</div>
																</div>
																<div class="profile-info-row">
																	<div class="profile-info-name"> Last Name </div>
																	<div class="profile-info-value">
																		<span><?=$result['profile_reg']['last_name']?></span>
																	</div>
																</div>
																<div class="profile-info-row">
																	<div class="profile-info-name"> Date Of Birth </div>

																	<div class="profile-info-value">
																		<span>
																			<?=date("d-M-Y",strtotime($result['profile_reg']['date_of_birth']))?>
																		</span>
																	</div>
																</div>
																<div class="profile-info-row">
																	<div class="profile-info-name"> Address </div>

																	<div class="profile-info-value">
																		<span>
																		<?=$result['profile_reg']['address'];?>
																		</span>
																	</div>
																</div>
																
															</div>
															
															<div class="hr hr-8 dotted"></div>
														</div><!-- /.col -->
													</div><!-- /.row -->

													<div class="space-20"></div>
													<div class="row">
														<?php 
														if(count($result['bank_details'])>0){
														?>
														<div class="col-md-6">
															<h3>Bank Details</h3>
															<table class="table  table-bordered table-hover">
																<tbody>
																	<tr>
																		<td>Name</td>
																		<td>
																			<?=$result['bank_details']['sp_name']?>
																		</td>
																	</tr>
																	<tr>
																		<td>Account Number</td>
																		<td>
																			<?=$result['bank_details']['account_number']?>
																		</td>
																	</tr>
																	<tr>
																		<td>IFSC Code</td>
																		<td>
																			<?=$result['bank_details']['ifsc_code']?>
																		</td>
																	</tr>
																	<tr>
																		<td>Cancel Cheque Photo</td>
																		<td>
																			<img src="<?=$result['bank_details']['cancel_cheque_photo']?>" style="width:100px; height:100px;"/></td>
																	</tr>
																</tbody>
															</table>
														
														</div>
														<?php 
														}
														?>
														<?php 
														if(count($result['gst_details'])>0){
														?>
														<div class="col-md-6">
															<h3>GST Details</h3>
															<table class="table  table-bordered table-hover">
																<tbody>
																	<tr>
																		<td>Pancard No.</td>
																		<td>
																			<?=$result['gst_details']['pancard_number'] ?>
																		</td>
																	</tr>
																	<tr>
																		<td>Name</td>
																		<td>
																			<?=$result['gst_details']['sp_name'] ?>
																		</td>
																	</tr>
																	<tr>
																		<td>GST No.</td>
																		<td><?=$result['gst_details']['gst_number'] ?></td>
																	</tr>
																	<tr>
																		<td>GST Photo</td>
																		<td>
																			<img src="<?=$result['gst_details']['gst_document_photo'] ?>" style="width:100px; height:100px;" />
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<?php 
														}
														?>
													</div>
												</div><!-- /#home -->
												<div id="dri_pro_info" class="tab-pane">
													<div class="col-md-1">&nbsp;</div>
													<div class="col-md-10">
														<table id="simple-table" class="table  table-bordered table-hover">
															<?php 
															if(count($result['driver_pro_qua'])>0){
															?>
															<tbody>
																<tr>
																	<td>Name</td>
																	<td>
																	<?=$result['driver_pro_qua']['name'];?>
																	</td>
																</tr>
																<tr>
																	<td>DL Number</td>
																	<td>
																		<?=$result['driver_pro_qua']['driving_license_number'];?>
																	</td>
																</tr>
																<tr>
																	<td>Photo</td>
																	<td>
																		<img src="<?=$result['driver_pro_qua']['dl_photo'];?>" style="width:80px; height:80px;" />
																	</td>
																</tr>
															</tbody>
															<?php 
															}
															?>
														</table>
													</div>
													<div class="col-md-1"></div>
												</div>
												<div id="labinfo" class="tab-pane">
													<div class="col-md-1">&nbsp;</div>
													<div class="col-md-10">
														<table id="simple-table" class="table  table-bordered table-hover">
															<?php 
															if(count($result['lab_info'])>0){
															?>
															<tbody>
																<tr>
																	<td>Name</td>
																	<td>
																	<?=$result['lab_info']['name'];?>
																	</td>
																</tr>
																<tr>
																	<td>Lab Photo</td>
																	<td>
																		<img src="<?=$result['lab_info']['lab_profile_picture'];?>" style="width:80px; height:80px;" />
																	</td>
																</tr>
																<tr>
																	<td>Lab Mobile No.</td>
																	<td>
																		<?=$result['lab_info']['lab_mobile_no'];?>
																	</td>
																</tr>
																<tr>
																	<td>Email ID.</td>
																	<td>
																		<?=$result['lab_info']['lab_email_id'];?>
																	</td>
																</tr>
																<tr>
																	<td>Address Proof Type</td>
																	<td>
																		<?=$result['lab_info']['lab_address_proof_type'];?>
																	</td>
																</tr>
																<tr>
																	<td>Address Proof Photo</td>
																	<td>
																		<img src="<?=$result['lab_info']['lab_upload_document'];?>" style="width:80px; height:80px;" />
																	</td>
																</tr>
															</tbody>
															<?php 
															}
															?>
														</table>
													</div>
													<div class="col-md-1"></div>
												</div>
												<div id="chemist" class="tab-pane">
													<div class="col-md-1">&nbsp;</div>
													<div class="col-md-10">
														<table id="simple-table" class="table  table-bordered table-hover">
															<?php 
															if(count($result['chemist'])>0){
															?>
															<tbody>
																<tr>
																	<td>Drug License No.</td>
																	<td>
																	<?=$result['chemist']['drug_licence_no'];?>
																	</td>
																</tr>
																<tr>
																	<td>DL Photo</td>
																	<td>
																		<img src="<?=$result['chemist']['drug_licence_photo'];?>" style="width:80px; height:80px;" />
																	</td>
																</tr>
																<tr>
																	<td>PAN No.</td>
																	<td>
																		<?=$result['chemist']['pan_no'];?>
																	</td>
																</tr>
																<tr>
																	<td>PAN Photo</td>
																	<td>
																		<img src="<?=$result['chemist']['pan_photo'];?>" style="width:80px; height:80px;" />
																	</td>
																</tr>
																<tr>
																	<td>GST No.</td>
																	<td>
																		<?=$result['chemist']['gst_no'];?>
																	</td>
																</tr>
																<tr>
																	<td>GST Photo</td>
																	<td>
																		<img src="<?=$result['chemist']['gst_document'];?>" style="width:80px; height:80px;" />
																	</td>
																</tr>
															</tbody>
															<?php 
															}
															?>
														</table>
													</div>
													<div class="col-md-1"></div>
												</div>
												<div id="vehicle_details" class="tab-pane">
													<div class="col-md-1">&nbsp;</div>
													<div class="col-md-10">
														<table id="simple-table" class="table  table-bordered table-hover">
															<thead>
																
																<th>Name</th>
																<th>Type</th>
																<th>Reg. Date</th>
																<th>Reg. No.</th>
																<th>Reg.no Photo</th>
															</thead>
															<tbody>
										<?php 
											if(count($result['vehicle_details'])>0){
												foreach($result['vehicle_details']['all_vehicles'] as $key => $value){
										?>
													<tr>
														<td><?=$value['vehicle_name']?></td>
														<td><?=$value['vehicle_type']?></td>
														<td><?=date("d-M-y",strtotime($value['registration_date']))?></td>
														<td><?=$value['vehicle_reg_no']?></td>
														<td>
							<img src="<?=$value['reg_certificate_photo']?>" style="width:50px; height:50px;"/>
														</td>
													</tr>
										<?php
												}
												
											}
										?>
															</tbody>
														</table>
													</div>
													<div class="col-md-1">&nbsp;</div>
												</div>
												<div id="dri_pro_qua" class="tab-pane">
													<div class="col-md-1">&nbsp;</div>
													<div class="col-md-10">
														<table id="simple-table" class="table  table-bordered table-hover">
															<thead>
																
																<th>Vehicle Name</th>
																<th>Driver Name</th>
															</thead>
															<tbody>
																<?php 
											if(count($result['vehicle_driver_details'])>0){
												foreach($result['vehicle_driver_details'] as $key1 => $value1){
										?>
													<tr>
														<td><?=$value1['vehicle_name']?></td>
														<td><?=$value1['driver_name']?></td>
														
													</tr>
										<?php
												}
												
											}
										?>
															</tbody>
														</table>
													</div>
													<div class="col-md-1">&nbsp;</div>
												</div>
												<div id="feed" class="tab-pane">
													<div class="profile-feed row">
														<div class="col-sm-12">
															<?php 
															if(count($result['sldata'])>0){
															?>
															<div class="profile-activity clearfix">
																<div class="profile-info-name"> Location Details </div>
																<div class="profile-info-value">
																	<span>
																	<?=$result['sldata']['service_location'];?>
																	</span>
																</div>
															</div>
															<div class="profile-activity clearfix">
																<div class="profile-info-name"> Distance </div>
																<div class="profile-info-value">
																	<span>
																	<?=$result['sldata']['service_distance'];?>
																	</span>
																</div>
															</div>
															<?php 
															}else{
																echo "Data Not Available...";
															}
															?>
														</div><!-- /.col -->
													</div><!-- /.row -->
												</div><!-- /#feed -->

												<div id="friends" class="tab-pane">
													<div class="profile-users clearfix">
														<div class="col-sm-12">
															<div class="card w-75">
																<div class="card-body">
																	<?php 
																	if(count($result['sp_qualification'])>0){
																	?>
																	<h5 class="card-title">
																		<strong>Qualification Name</strong> :
																		<?=$result['sp_qualification']['qualification_name']?>
																	</h5>
																	<h5 class="card-title">
																		<strong>Additional Details</strong>(If Any) :
																		<?=$result['sp_qualification']['additional_qualification_details']?>
																	</h5>
																	<h5 class="card-title">
																		<strong>Registration No. </strong> :
																		<?=$result['sp_qualification']['registration_number']?>
																	</h5>
																	<h5 class="card-title">
																		<strong>Year of Exp.. </strong> :
																		<?=$result['sp_qualification']['year_of_experience']?> Yrs.
																	</h5>
																	<h5 class="card-title">
																		<strong>Attachment File </strong> :
																		<img src="<?=$result['sp_qualification']['certification_attachment']?>" style="width: 450px; height:300px;"/>
																	</h5>
																	<?php 
																	}else{
																		echo "Data Not Available...";
																	}
																	?>
																</div>
															</div>
														</div>
													</div>
												</div><!-- /#friends -->
												
												<div id="dri_pro_qua" class="tab-pane">
													<div class="profile-users clearfix">
														<div class="col-sm-12">
															<div class="card w-75">
																<?php 
																if(count($result['driver_pro_qua']) > 0){
																?>
																<div class="card-body">
																	<h5 class="card-title">
																		<strong>Name</strong> :
																		<?=$result['driver_pro_qua']['name']?>
																	</h5>
																	<h5 class="card-title">
																		<strong>D.L. Number</strong> :
																		<?=$result['driver_pro_qua']['driving_license_number']?>
																	</h5>
																	
																	<h5 class="card-title">
																		<strong>Front Photo </strong> :
																		<img src="<?=$result['driver_pro_qua']['dl_photo']?>" style="width: 250px; height:200px;"/>
																	</h5>
																</div>
																<?php 
																}else{
																?>
																<div class="card-body">
																	<h5 class="card-title">Data Not Available...</h5>
																</div>
																<?php
																}
																?>
															
															</div>
														</div>
													</div>
												</div>
											
												<div id="pictures" class="tab-pane">
													<div class="profile-users clearfix">
														<div class="col-sm-12">
															<div class="card w-75">
																<?php 
																if(count($result['identityverification']) > 0){
																?>
																<div class="card-body">
																	<h5 class="card-title">
																		<strong>Identification Name</strong> :
																		<?=$result['identityverification']['identification_name']?>
																	</h5>
																	<h5 class="card-title">
																		<strong>Identification No.</strong> :
																		<?=$result['identityverification']['identification_number']?>
																	</h5>
																	
																	<h5 class="card-title">
																		<strong>Front Photo </strong> :
																		<img src="<?=$result['identityverification']['identification_front_photo']?>" style="width: 250px; height:200px;"/>
																	</h5>
																	<h5 class="card-title">
																		<strong>Back Photo </strong> :
																		<img src="<?=$result['identityverification']['identification_back_photo']?>" style="width: 250px; height:200px;"/>
																	</h5>
																	
																</div>
																<?php 
																}else{
																?>
																<div class="card-body">
																	<h5 class="card-title">Data Not Available...</h5>
																</div>
																<?php
																}
																?>
															
															</div>
														</div>
													</div>
												</div>
											
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										
										<div class="col-md-4">
											<button type="button" class="btn btn-info" onclick="acceptRegistration(<?=$result['profile_reg']['profile_id'];?>);" <?=$approve_btn_status;?> >
												<span class="glyphicon glyphicon-check"></span> Approve
											</button>
										</div>							
																	
										<div class="col-md-4" style="text-align:center;">
											<button type="button" class="btn btn-success" onclick="complete_verification(<?=$result['profile_reg']['profile_id'];?>);" >
												<span class="glyphicon glyphicon-ok"></span>
												Complete
											</button>
										</div>							
										<div class="col-md-4" style="text-align:right;">
											<button type="button" class="btn btn-danger" onclick="rejectRegistration(<?=$result['profile_reg']['profile_id'];?>);" <?=$cancel_btn_status;?> >
												<span class="glyphicon glyphicon-remove"></span> Reject
											</button>
										</div>							
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php include('include/footer.php')?>
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">
			  <!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Verification Datetime set</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class='col-sm-12'>
								<div class="form-group">
									<div class='input-group date' id='datetimepicker1'>
										<input type='text' name='choose_datetime' class="form-control" id='choose_datetime' />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
								<div class="form-group">
									<input type="text" name="address" id="address" placeholder="Enter the address" class="form-control" />
								</div>
								<button type="button" class="btn btn-default" onclick="setverification(<?=$result['profile_reg']['profile_id'];?>)">Save</button>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<?php include('include/footer_js.php')?>
		<script>
			var base_url = '<?=base_url();?>';
			function acceptRegistration(profile_id){
				if(profile_id!=''){
					$('#choose_datetime').val('');
					$('#address').val('');
					$("#myModal").modal('show');
				}else{
					alertify.alert('Profile ID can ot be empty.', function(){
						console.log('error');
					}).setHeader('VKAREU'); ;
					return false;
				}

			}
			
			function setverification(profile_id){
				var choosen_datetime = $('#choose_datetime').val();
				var address = $('#address').val();
				if(choosen_datetime != ''){
					if(address!=''){
						$.ajax({
							url: base_url+'dashboard/setfacetofaceverification',
							type: "POST",
							data: {profile_id : profile_id,choosen_datetime:choosen_datetime,address:address},
							beforeSend:function(json)
							{
								$('.loading').show();
							},
							success: function (result) {
								var json_res = JSON.parse(result);
								alertify.alert(json_res.msg, function(){
									location.reload();
								}).setHeader('VKAREU');
								//location.reload();
							},
							complete:function(json)
							{
								$('.loading').hide();
							},
						});
					}else{
						//alertify.alert('Please choose Address..!').setHeader('VKAREU');
						if(!alertify.errorAlert){
							alertify.dialog('errorAlert',function factory(){
								return{
									build:function(){
										var errorHeader = '<span class="fa fa-times-circle fa-2x" '
										+    'style="vertical-align:middle;color:#e10000;">'
										+ '</span> VKAREU';
										this.setHeader(errorHeader);
									}
								};
							},true,'alert');
						}
						alertify.errorAlert("Please choose Address..!");
						return false;
					}
				}else{
					if(!alertify.errorAlert){
						alertify.dialog('errorAlert',function factory(){
							return{
								build:function(){
									var errorHeader = '<span class="fa fa-times-circle fa-2x" '
									+    'style="vertical-align:middle;color:#e10000;">'
									+ '</span> VKAREU';
									this.setHeader(errorHeader);
								}
							};
						},true,'alert');
					}
					alertify.errorAlert("Please choose the datetime...");
					return false;
				}
			}
			function rejectRegistration(profile_id){
				
				alertify.confirm("Are you sure want to reject the verification request..", function(){
					$.ajax({
						url: base_url+'dashboard/rejectVerification',
						type: "POST",
						data: {profile_id : profile_id},
						beforeSend:function(json)
						{
							$('.loading').show();
						},
						success: function (result) {
							var json_res = JSON.parse(result);
							alertify.alert(json_res.msg, function(){
								location.reload();
							}).setHeader('VKAREU');
						},
						complete:function(json)
						{
							$('.loading').hide();
						},
					});
				},function(){
					console.log('cancel');
				}).setHeader('VKAREU');;
				
				
			}
			function setFacetoface(profile_id){
				if(profile_id!=''){
					$('#choose_datetime').val('');
					$('#address').val('');
					$("#myModal").modal('show');
				}else{
					alertify.alert('Profile ID can not be empty', function(){
						console.log('error');
					}).setHeader('VKAREU'); ;
					return false;
				}
			}
			function complete_verification(profile_id){
				if(profile_id!=''){
					$.ajax({
						url: base_url+'dashboard/completeVerification',
						type: "POST",
						data: {profile_id : profile_id},
						beforeSend:function(json)
						{
							$('.loading').show();
						},
						success: function (result) {
							var json_res = JSON.parse(result);
							alertify.alert(json_res.msg, function(){
								location.reload();
							}).setHeader('VKAREU');
						},
						complete:function(json)
						{
							$('.loading').hide();
						},
					});
				}else{
					alertify.alert('Profile ID can not be empty..', function(){
						console.log('error');
					}).setHeader('VKAREU');
					return false;
				}
			}
			$(function () {
				$('#datetimepicker1').datetimepicker();
			});
		</script>
	</body>
</html>
