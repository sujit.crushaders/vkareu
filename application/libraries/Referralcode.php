<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referralcode{
        public function generate(){
			$chars = "123456789AaBbCcDdEeFfGgHhIJKkLMmNnPpQqRrSsTtUuVvWwXxYyZz";
			$res = "";
			for ($i = 0; $i < 5; $i++) {
				$res .= $chars[mt_rand(0, strlen($chars)-1)];
			}

			return $res;
		}
}
