<?php 
class Spprofileimage_model extends CI_Model {
	private $table;
    public function __construct()
    {
		$this->load->database();
		$this->table = "sp_profile_image";
		$this->load->model('spprofileimage_model');
		$this->load->model('serviceslist_model');
	}
	public function showprofileimage($id){
		$profile_data = $this->db->get_where("sp_profile_image",array('id'=>$id))->row();
		if($profile_data){
			$path = $profile_data->profile_image;
		}else{
			$path = base_url()."assets/images/no_image.png";
		}
		return $path;
	}
	
	public function getcompletepercentage($id){
		$complete = $this->db->query("select * from spregistration_steps where service_provider_id='$id'")->row();
		if($complete){
			$percentage = $complete->complete_percentage;
		}else{
			$percentage = 0.00;
		}
		
		return $percentage;
	}
	
	public function getagencystatus($id){
		$agency_status = $this->db->query("select * from spregistration_steps where service_provider_id='$id'")->row();
		if($agency_status){
			if($agency_status->isverified == 0){
				$status = "Request not <br/>Sent";
			}else if($agency_status->isverified == 1){
				$status = "Request Sent";
			}else if($agency_status->isverified == 2){
				$status = "Physical Verification <br/> Schedule";
			}else if($agency_status->isverified == -1){
				$status = "Request rejected..";
			}else{
				$status = "Profile verification <br/> Completed..";
			}
		}else{
			$status = 0.00;
		}
		
		return $status;
	}
	
	public function getServicetype($id){
		$service_details = $this->db->get_where('serviceslist',array('id'=>$id))->row();
		if($service_details){
			$name = $service_details->name;
		}else{
			$name = '';
		}
		return $name;
	}
	
	public function isverified($id){
		$details = $this->db->query("select * from spregistration_steps where service_provider_id='$id'")->row();
		if($details){
			if($details->isverified == 0){
				$status = "Pending";
			}else if($details->isverified == 1){
				$status = "Verified";
			}else{
				$status = "Reject";
			}
		}else{
			$status = "Not Available..";
		}
		
		return $status;
	}
	public function agencyStatus($id){
		$details = $this->db->query("select * from spregistration_steps where service_provider_id='$id'")->row();
		if($details){
			$status = $details->isverified;
		}else{
			$status = "";
		}
		
		return $status;
	}
}

?>