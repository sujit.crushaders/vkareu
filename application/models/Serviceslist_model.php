<?php 
class Serviceslist_model extends CI_Model {
	private $table;
    public function __construct()
    {
		$this->load->database();
		$this->table = "serviceslist";
	}

	public function service_name($service_id){
		$service_details = $this->db->get_where('serviceslist',array('id'=>$service_id))->row();
		if($service_details){
			$service_name = $service_details->name;
		}else{
			$service_name = "";
		}
		return $service_name;
	}
}

?>