-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 26, 2020 at 03:21 PM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ctslpgr8_vkareu`
--

-- --------------------------------------------------------

--
-- Table structure for table `ambdrivervehiclemaping`
--

CREATE TABLE `ambdrivervehiclemaping` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `ambvehicledetails_id` int(11) NOT NULL,
  `ambulance_driver_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ambulancedriverprofessional`
--

CREATE TABLE `ambulancedriverprofessional` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `driving_license_number` varchar(100) NOT NULL,
  `dl_photo` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ambvehicledetails`
--

CREATE TABLE `ambvehicledetails` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `vehicle_name` varchar(200) NOT NULL,
  `vehicle_type` varchar(50) NOT NULL,
  `registration_date` date NOT NULL,
  `vehicle_reg_no` varchar(100) NOT NULL,
  `reg_certificate_photo` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ambvehicledetails`
--

INSERT INTO `ambvehicledetails` (`id`, `service_provider_id`, `vehicle_name`, `vehicle_type`, `registration_date`, `vehicle_reg_no`, `reg_certificate_photo`, `create_date`, `update_date`, `status`) VALUES
(1, 1, 'tyu', 'AC', '1991-01-01', 'cvhhjjk5677', '1', '2020-01-26 12:27:48', '2020-01-26 12:27:48', 1),
(2, 1, 'dfghj', 'AC', '2000-01-01', 'dghjjkk', '2', '2020-01-26 12:27:49', '2020-01-26 12:27:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chemistidentityverification`
--

CREATE TABLE `chemistidentityverification` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `shop_name` varchar(100) NOT NULL,
  `shop_photo` text NOT NULL,
  `shop_address_proof_type` varchar(150) NOT NULL,
  `address_proof_document` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chemistshopinformation`
--

CREATE TABLE `chemistshopinformation` (
  `id` int(11) NOT NULL,
  `drug_licence_no` varchar(50) NOT NULL,
  `drug_licence_photo` text NOT NULL,
  `pan_no` varchar(50) NOT NULL,
  `pan_photo` text NOT NULL,
  `gst_no` varchar(50) NOT NULL,
  `gst_document` text NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `labinformation`
--

CREATE TABLE `labinformation` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `lab_name` varchar(100) NOT NULL,
  `lab_profile_picture` text NOT NULL,
  `lab_mobile_no` varchar(15) NOT NULL,
  `lab_email_id` varchar(100) NOT NULL,
  `lab_address_proof_type` varchar(100) NOT NULL,
  `lab_upload_document` text NOT NULL,
  `lab_lat` varchar(100) DEFAULT NULL,
  `lab_lang` varchar(100) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otp_verification`
--

CREATE TABLE `otp_verification` (
  `id` int(11) NOT NULL,
  `mobile_no` bigint(20) NOT NULL,
  `otp` bigint(20) NOT NULL,
  `isverified` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelete` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp_verification`
--

INSERT INTO `otp_verification` (`id`, `mobile_no`, `otp`, `isverified`, `create_date`, `update_date`, `isdelete`) VALUES
(1, 7978902087, 5512, 1, '2020-01-25 18:49:37', '2020-01-25 18:49:45', 1),
(2, 8249300712, 6230, 1, '2020-01-26 12:22:14', '2020-01-26 12:22:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelete` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `type`, `create_date`, `update_date`, `isdelete`) VALUES
(1, 'Admin', '2019-12-30 19:44:25', '2019-12-30 19:44:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `serviceprovider_profile_registration`
--

CREATE TABLE `serviceprovider_profile_registration` (
  `id` int(11) NOT NULL,
  `servicetype_id` int(11) NOT NULL COMMENT 'serviceslist_id',
  `mobile_no` bigint(20) NOT NULL,
  `first_name` varchar(250) NOT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) NOT NULL,
  `gender` int(11) NOT NULL COMMENT '1->Male,2->Female',
  `date_of_birth` date NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address` text NOT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `profileimage_id` int(11) DEFAULT NULL COMMENT 'sp_profile_image -> id',
  `is_declaration` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `serviceprovider_profile_registration`
--

INSERT INTO `serviceprovider_profile_registration` (`id`, `servicetype_id`, `mobile_no`, `first_name`, `middle_name`, `last_name`, `gender`, `date_of_birth`, `email`, `address`, `latitude`, `longitude`, `profileimage_id`, `is_declaration`, `create_date`, `update_date`, `status`) VALUES
(1, 6, 8249300712, 'arghya', '', 'pal', 1, '1970-01-01', '', 'patia', '20.2777849', '85.8371757', 0, 1, '2020-01-26 12:25:36', '2020-01-26 12:32:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `serviceslist`
--

CREATE TABLE `serviceslist` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelete` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `serviceslist`
--

INSERT INTO `serviceslist` (`id`, `name`, `create_date`, `update_date`, `isdelete`) VALUES
(1, 'Doctor', '2020-01-03 12:22:17', '2020-01-20 07:32:58', 1),
(2, 'Nurse', '2020-01-03 12:22:34', '2020-01-20 07:33:04', 1),
(3, 'Physiotherapy', '2020-01-03 15:24:35', '2020-01-03 15:24:35', 1),
(4, 'Pharmacist', '2020-01-03 15:24:35', '2020-01-03 15:24:35', 1),
(5, 'Chemist', '2020-01-04 14:47:23', '2020-01-19 08:14:01', 1),
(6, 'Ambulance Owner', '2020-01-04 15:22:46', '2020-01-19 08:14:28', 1),
(7, 'Ambulance Driver', '2020-01-04 15:23:13', '2020-01-19 08:14:37', 1),
(8, 'Lab Owner', '2020-01-17 10:24:09', '2020-01-19 08:15:06', 1),
(9, 'Lab Technician', '2020-01-17 10:24:09', '2020-01-19 08:15:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `spbankdetails`
--

CREATE TABLE `spbankdetails` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `sp_name` varchar(200) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `ifsc_code` varchar(100) NOT NULL,
  `cancel_cheque_photo` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spbankdetails`
--

INSERT INTO `spbankdetails` (`id`, `service_provider_id`, `sp_name`, `account_number`, `ifsc_code`, `cancel_cheque_photo`, `create_date`, `update_date`, `status`) VALUES
(1, 1, 'arghya', '53698844568', 'gjkvbnnvxbn', '6', '2020-01-26 12:38:27', '2020-01-26 12:38:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `spidentity_verification`
--

CREATE TABLE `spidentity_verification` (
  `id` int(11) NOT NULL,
  `identification_name` text NOT NULL,
  `identification_number` text NOT NULL,
  `identification_front_photo` text NOT NULL,
  `identification_back_photo` text NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spidentity_verification`
--

INSERT INTO `spidentity_verification` (`id`, `identification_name`, `identification_number`, `identification_front_photo`, `identification_back_photo`, `service_provider_id`, `create_date`, `update_date`, `status`) VALUES
(1, 'PAN', 'fgkkbn', '4', '5', 1, '2020-01-26 12:31:49', '2020-01-26 12:31:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sppancard`
--

CREATE TABLE `sppancard` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `pancard_number` varchar(100) NOT NULL,
  `sp_name` varchar(100) NOT NULL,
  `gst_number` varchar(100) NOT NULL,
  `gst_document_photo` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `spqualification`
--

CREATE TABLE `spqualification` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `qualification_name` varchar(100) NOT NULL,
  `other_qualification_details` text,
  `additional_qualification` text NOT NULL,
  `registration_number` varchar(50) NOT NULL,
  `certification_attachment` text NOT NULL,
  `year_of_experience` float(10,2) NOT NULL COMMENT 'experience in year',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spqualification`
--

INSERT INTO `spqualification` (`id`, `service_provider_id`, `qualification_name`, `other_qualification_details`, `additional_qualification`, `registration_number`, `certification_attachment`, `year_of_experience`, `create_date`, `update_date`, `status`) VALUES
(1, 1, 'MBBS', '', '', 'dgbmmvbnm', '3', 1.00, '2020-01-26 12:30:43', '2020-01-26 12:30:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `spregistration_steps`
--

CREATE TABLE `spregistration_steps` (
  `id` int(11) NOT NULL,
  `steps_complete` int(11) NOT NULL COMMENT '0->Not Start,1->Profile Process,2->Service location,3->Qualification,4->identification verification,5->declation',
  `service_provider_id` int(11) NOT NULL,
  `complete_percentage` float(10,2) NOT NULL,
  `isverified` int(11) NOT NULL DEFAULT '0' COMMENT '0->Pending,1->send request for verification,2->Waiting for Physicalverification,3->Verification Completed,-1->Request Rejected...',
  `facetofaceverification` datetime DEFAULT NULL,
  `address_facetoface` text,
  `datetime_rejection` datetime DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spregistration_steps`
--

INSERT INTO `spregistration_steps` (`id`, `steps_complete`, `service_provider_id`, `complete_percentage`, `isverified`, `facetofaceverification`, `address_facetoface`, `datetime_rejection`, `create_date`, `update_date`, `status`) VALUES
(1, 5, 1, 100.00, 1, NULL, NULL, NULL, '2020-01-26 12:25:36', '2020-01-26 12:32:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `spservice_location`
--

CREATE TABLE `spservice_location` (
  `id` int(11) NOT NULL,
  `service_location` text NOT NULL,
  `service_distance` float(10,2) NOT NULL COMMENT 'distance in k.m',
  `location_lat` varchar(50) NOT NULL,
  `location_lang` varchar(50) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spservice_location`
--

INSERT INTO `spservice_location` (`id`, `service_location`, `service_distance`, `location_lat`, `location_lang`, `service_provider_id`, `create_date`, `update_date`, `status`) VALUES
(1, '2RA(8/2), UNIT- 9, Bhubaneswar, Odisha 751022, India,Bhubaneswar,Odisha,India', 10.00, '20.287224335582273', '85.83880919963121', 1, '2020-01-26 12:28:50', '2020-01-26 12:28:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sp_profile_image`
--

CREATE TABLE `sp_profile_image` (
  `id` int(11) NOT NULL,
  `profile_image` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sp_profile_image`
--

INSERT INTO `sp_profile_image` (`id`, `profile_image`, `create_date`, `update_date`, `status`) VALUES
(1, 'http://demo.ctslproject.com/vkareu/upload/1580041668.png', '2020-01-26 12:27:48', '2020-01-26 12:27:48', 1),
(2, 'http://demo.ctslproject.com/vkareu/upload/1580041668.png', '2020-01-26 12:27:49', '2020-01-26 12:27:49', 1),
(3, 'http://demo.ctslproject.com/vkareu/upload/1580041843.png', '2020-01-26 12:30:43', '2020-01-26 12:30:43', 1),
(4, 'http://demo.ctslproject.com/vkareu/upload/1580041909.png', '2020-01-26 12:31:49', '2020-01-26 12:31:49', 1),
(5, 'http://demo.ctslproject.com/vkareu/upload/3240980981580041909.png', '2020-01-26 12:31:49', '2020-01-26 12:31:49', 1),
(6, 'http://demo.ctslproject.com/vkareu/upload/1580042307.png', '2020-01-26 12:38:27', '2020-01-26 12:38:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isdelete` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `role_id`, `create_date`, `update_date`, `isdelete`) VALUES
(1, 'vkareu admin', 'vkareu@gmail.com', 'c86bffc92c4ec8eeadba8499abf48c1722418754', 1, '2019-12-30 19:44:35', '2019-12-31 11:43:02', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ambdrivervehiclemaping`
--
ALTER TABLE `ambdrivervehiclemaping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ambulancedriverprofessional`
--
ALTER TABLE `ambulancedriverprofessional`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `ambvehicledetails`
--
ALTER TABLE `ambvehicledetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `chemistidentityverification`
--
ALTER TABLE `chemistidentityverification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `chemistshopinformation`
--
ALTER TABLE `chemistshopinformation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `labinformation`
--
ALTER TABLE `labinformation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `otp_verification`
--
ALTER TABLE `otp_verification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `serviceprovider_profile_registration`
--
ALTER TABLE `serviceprovider_profile_registration`
  ADD PRIMARY KEY (`id`),
  ADD KEY `servicetype_id` (`servicetype_id`);

--
-- Indexes for table `serviceslist`
--
ALTER TABLE `serviceslist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spbankdetails`
--
ALTER TABLE `spbankdetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `spidentity_verification`
--
ALTER TABLE `spidentity_verification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `sppancard`
--
ALTER TABLE `sppancard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `spqualification`
--
ALTER TABLE `spqualification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `spregistration_steps`
--
ALTER TABLE `spregistration_steps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `spservice_location`
--
ALTER TABLE `spservice_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `sp_profile_image`
--
ALTER TABLE `sp_profile_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ambdrivervehiclemaping`
--
ALTER TABLE `ambdrivervehiclemaping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ambulancedriverprofessional`
--
ALTER TABLE `ambulancedriverprofessional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ambvehicledetails`
--
ALTER TABLE `ambvehicledetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chemistidentityverification`
--
ALTER TABLE `chemistidentityverification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chemistshopinformation`
--
ALTER TABLE `chemistshopinformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `labinformation`
--
ALTER TABLE `labinformation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `otp_verification`
--
ALTER TABLE `otp_verification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `serviceprovider_profile_registration`
--
ALTER TABLE `serviceprovider_profile_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `serviceslist`
--
ALTER TABLE `serviceslist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `spbankdetails`
--
ALTER TABLE `spbankdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `spidentity_verification`
--
ALTER TABLE `spidentity_verification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sppancard`
--
ALTER TABLE `sppancard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `spqualification`
--
ALTER TABLE `spqualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `spregistration_steps`
--
ALTER TABLE `spregistration_steps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `spservice_location`
--
ALTER TABLE `spservice_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sp_profile_image`
--
ALTER TABLE `sp_profile_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ambulancedriverprofessional`
--
ALTER TABLE `ambulancedriverprofessional`
  ADD CONSTRAINT `ambulancedriverprofessional_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ambvehicledetails`
--
ALTER TABLE `ambvehicledetails`
  ADD CONSTRAINT `ambvehicledetails_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chemistidentityverification`
--
ALTER TABLE `chemistidentityverification`
  ADD CONSTRAINT `chemistidentityverification_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chemistshopinformation`
--
ALTER TABLE `chemistshopinformation`
  ADD CONSTRAINT `chemistshopinformation_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `labinformation`
--
ALTER TABLE `labinformation`
  ADD CONSTRAINT `labinformation_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `serviceprovider_profile_registration`
--
ALTER TABLE `serviceprovider_profile_registration`
  ADD CONSTRAINT `serviceprovider_profile_registration_ibfk_1` FOREIGN KEY (`servicetype_id`) REFERENCES `serviceslist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `spbankdetails`
--
ALTER TABLE `spbankdetails`
  ADD CONSTRAINT `spbankdetails_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `spidentity_verification`
--
ALTER TABLE `spidentity_verification`
  ADD CONSTRAINT `spidentity_verification_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sppancard`
--
ALTER TABLE `sppancard`
  ADD CONSTRAINT `sppancard_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `spqualification`
--
ALTER TABLE `spqualification`
  ADD CONSTRAINT `spqualification_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `spregistration_steps`
--
ALTER TABLE `spregistration_steps`
  ADD CONSTRAINT `spregistration_steps_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`);

--
-- Constraints for table `spservice_location`
--
ALTER TABLE `spservice_location`
  ADD CONSTRAINT `spservice_location_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `serviceprovider_profile_registration` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
